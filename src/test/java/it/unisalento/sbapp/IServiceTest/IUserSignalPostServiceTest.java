package it.unisalento.sbapp.IServiceTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.unisalento.sbapp.DomainClasses.*;
import it.unisalento.sbapp.Exception.SavingUserSignalPostException;
import it.unisalento.sbapp.Exception.UserSignalPostNotFoundException;
import it.unisalento.sbapp.Iservice.IUserSignalPostService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class IUserSignalPostServiceTest {

	@Mock
	IUserSignalPostService iUserSignalPostServiceMock;

	UserSignalPost front_end;
	int id;

	@BeforeEach
	void init() {
		front_end = new UserSignalPost(0, new User(), new Post(), "Ragione segnalazione", new Date());
		try {
			when(iUserSignalPostServiceMock.save(front_end)).thenReturn(front_end);
			when(iUserSignalPostServiceMock.save(null)).thenThrow(SavingUserSignalPostException.class);
			when(iUserSignalPostServiceMock.getById(100)).thenReturn(front_end);
			when(iUserSignalPostServiceMock.getById(0)).thenThrow(UserSignalPostNotFoundException.class);
			when(iUserSignalPostServiceMock.getAll()).thenReturn(new ArrayList<UserSignalPost>());
			when(iUserSignalPostServiceMock.delete(0)).thenReturn(true);
			when(iUserSignalPostServiceMock.findUserSignaledPostByIdPost(id)).thenReturn(new ArrayList<UserSignalPost>());
			when(iUserSignalPostServiceMock.findUserSignaledPostByIdPost(0)).thenThrow(UserSignalPostNotFoundException.class);
			when(iUserSignalPostServiceMock.findPostSignaledByUser(id)).thenReturn(new ArrayList<UserSignalPost>());
			when(iUserSignalPostServiceMock.findPostSignaledByUser(0)).thenThrow(UserSignalPostNotFoundException.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	void saveTest() {
		id = 100; // valore falso

		try {
			id = iUserSignalPostServiceMock.save(front_end).getId();
		} catch (SavingUserSignalPostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertThat(id).isEqualTo(0);

	}

	@Test
	void saveThrowsExTest() {
		Exception eccezione = assertThrows(SavingUserSignalPostException.class, () -> {
			// faccio una chiamata che sicuramente mi genera eccezzione , infatti ID=0 non
			// esiste nel DB
			Object rit = iUserSignalPostServiceMock.save(null);
			if (rit == null)
				throw new SavingUserSignalPostException();
		});
		assertThat(eccezione).isNotNull();
	}

	@Test
	void getByIdTest() {
		try {
			id = 100;
			assertThat(iUserSignalPostServiceMock.getById(id)).isNotNull();
		} catch (UserSignalPostNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	void getByIdThrowsExTest() {
		Exception eccezione = assertThrows(UserSignalPostNotFoundException.class, () -> {
			// faccio una chiamata che sicuramente mi genera eccezzione , infatti ID=0 non
			// esiste nel DB
			 iUserSignalPostServiceMock.getById(0);
			
		});
		assertThat(eccezione).isNotNull();
	}

	@Test
	void getAllTest() {
		assertThat(iUserSignalPostServiceMock.getAll()).isNotNull();
	}

	@Test
	void deleteTest() {
		 id = 100;
		try {
			assertThat(iUserSignalPostServiceMock.delete(0)).isTrue();
			
		} catch (UserSignalPostNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	void findUserSignaledPostByIdPostTest() {
		int id = 100;
		try {
			assertThat(iUserSignalPostServiceMock.findUserSignaledPostByIdPost(id)).isNotNull();
		} catch (UserSignalPostNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	void findUserSignaledPostByIdPostThrowsExTest() {
		Exception eccezione = assertThrows(UserSignalPostNotFoundException.class, () -> {
			// faccio una chiamata che sicuramente mi genera eccezzione , infatti ID=0 non
			// esiste nel DB
			 iUserSignalPostServiceMock.findUserSignaledPostByIdPost(0);
			
		});
		assertThat(eccezione).isNotNull();
	}

	@Test
	void findPostSignaledByUserIdTest() {
		int id = 100;
		try {
			assertThat(iUserSignalPostServiceMock.findPostSignaledByUser(id)).isNotNull();
		} catch (UserSignalPostNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	void findPostSignaledByUserThrowsExTest() {
		Exception eccezione = assertThrows(UserSignalPostNotFoundException.class, () -> {
			// faccio una chiamata che sicuramente mi genera eccezzione , infatti ID=0 non
			// esiste nel DB
			 iUserSignalPostServiceMock.findPostSignaledByUser(0);
			
		});
		assertThat(eccezione).isNotNull();
	}
}
