package it.unisalento.sbapp.IServiceTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.unisalento.sbapp.DomainClasses.Post;
import it.unisalento.sbapp.DomainClasses.User;
import it.unisalento.sbapp.DomainClasses.UserViewPost;
import it.unisalento.sbapp.Exception.SavingUserViewPostException;

import it.unisalento.sbapp.Exception.UserViewPostNotFoundException;
import it.unisalento.sbapp.Iservice.IUserViewPostService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class IUserViewPostServiceTest {

	@Mock
	IUserViewPostService iUserViewPostServiceMock;

	UserViewPost userViewPostFrontEnd;

	int id;

	@BeforeEach
	void init() {
		userViewPostFrontEnd = new UserViewPost();
		userViewPostFrontEnd.setPost(new Post());
		userViewPostFrontEnd.setUser(new User());
		userViewPostFrontEnd.setId(0);
		
		// informazioni sul comportamento dell'oggetto mock
		try {
			when(iUserViewPostServiceMock.save(userViewPostFrontEnd)).thenReturn(userViewPostFrontEnd);
			when(iUserViewPostServiceMock.save(null)).thenReturn(null);
			when(iUserViewPostServiceMock.getById(100)).thenReturn(userViewPostFrontEnd);
			when(iUserViewPostServiceMock.getById(0)).thenReturn(null);
			when(iUserViewPostServiceMock.getAll()).thenReturn(new ArrayList<UserViewPost>());
			when(iUserViewPostServiceMock.delete(0)).thenReturn(true);
			when(iUserViewPostServiceMock.findByPostId(id)).thenReturn(new ArrayList<UserViewPost>());
			when(iUserViewPostServiceMock.findByPostId(0)).thenReturn(null);
			when(iUserViewPostServiceMock.findByUserId(id)).thenReturn(new ArrayList<UserViewPost>());
			when(iUserViewPostServiceMock.findByUserId(0)).thenReturn(null);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	void saveTest() {
		int id = 100; // valore falso

		try {
			id = iUserViewPostServiceMock.save(userViewPostFrontEnd).getId();
		} catch (SavingUserViewPostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertThat(id).isEqualTo(0);

	}

	@Test
	void saveThrowsExTest() {
		Exception eccezione = assertThrows(SavingUserViewPostException.class, () -> {
			// faccio una chiamata che sicuramente mi genera eccezzione , infatti ID=0 non
			// esiste nel DB
			Object rit = iUserViewPostServiceMock.save(null);
			if (rit == null)
				throw new SavingUserViewPostException();
		});
		assertThat(eccezione).isNotNull();
	}

	@Test
	void getByIdTest() {
		try {
			int id = 100;
			assertThat(iUserViewPostServiceMock.getById(id)).isNotNull();
		} catch (UserViewPostNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	void getByIdThrowsExTest() {
		Exception eccezione = assertThrows(UserViewPostNotFoundException.class, () -> {
			// faccio una chiamata che sicuramente mi genera eccezzione , infatti ID=0 non
			// esiste nel DB
			Object rit = iUserViewPostServiceMock.getById(0);
			if (rit == null)
				throw new UserViewPostNotFoundException();
		});
		assertThat(eccezione).isNotNull();
	}

	@Test
	void getAllTest() {
		assertThat(iUserViewPostServiceMock.getAll()).isNotNull();
	}

	@Test
	void deleteTest() {
		int id = 100;
		try {
			assertThat(iUserViewPostServiceMock.delete(0)).isTrue();
			
		} catch (UserViewPostNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	void findByPostIdTest() {
		int id = 100;
		try {
			assertThat(iUserViewPostServiceMock.findByPostId(id)).isNotNull();
		} catch (UserViewPostNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	void findByPostIdThrowsExTest() {
		Exception eccezione = assertThrows(UserViewPostNotFoundException.class, () -> {
			// faccio una chiamata che sicuramente mi genera eccezzione , infatti ID=0 non
			// esiste nel DB
			Object rit = iUserViewPostServiceMock.findByPostId(0);
			if (rit == null)
				throw new UserViewPostNotFoundException();
		});
		assertThat(eccezione).isNotNull();
	}

	@Test
	void findByUserIdTest() {
		int id = 100;
		try {
			assertThat(iUserViewPostServiceMock.findByUserId(id)).isNotNull();
		} catch (UserViewPostNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	void findByUserIdThrowsExTest() {
		Exception eccezione = assertThrows(UserViewPostNotFoundException.class, () -> {
			// faccio una chiamata che sicuramente mi genera eccezzione , infatti ID=0 non
			// esiste nel DB
			Object rit = iUserViewPostServiceMock.findByPostId(0);
			if (rit == null)
				throw new UserViewPostNotFoundException();
		});
		assertThat(eccezione).isNotNull();
	}
}
