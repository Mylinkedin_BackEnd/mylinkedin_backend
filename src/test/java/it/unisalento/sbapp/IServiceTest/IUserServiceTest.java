package it.unisalento.sbapp.IServiceTest;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.unisalento.sbapp.DomainClasses.*;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Iservice.IUserService;
import it.unisalento.sbapp.Iservice.IUserService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class IUserServiceTest {
	
	@Mock
	IUserService iUserServiceMock;

	User front_end;
	int id;
	String name;
	
	@BeforeEach
	void init() {
		front_end = new User();
		
		try {
			when(iUserServiceMock.save(front_end)).thenReturn(front_end);
			when(iUserServiceMock.save(null)).thenReturn(null);
			when(iUserServiceMock.getById(100)).thenReturn(front_end);
			when(iUserServiceMock.getById(0)).thenReturn(null);
			when(iUserServiceMock.getAll()).thenReturn(new ArrayList<User>());
			when(iUserServiceMock.delete(0)).thenReturn(true);
			when(iUserServiceMock.getByName(name)).thenReturn(new ArrayList<User>());
			when(iUserServiceMock.getByName(name)).thenThrow(UserNotFoundException.class);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
