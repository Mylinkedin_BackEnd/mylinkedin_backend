package it.unisalento.sbapp.ServiceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unisalento.sbapp.DomainClasses.UserSelectPost;
import it.unisalento.sbapp.Exception.PostNotFoundException;
import it.unisalento.sbapp.Exception.SavingUserSelectPostException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Exception.UserSelectPostNotFoundException;
import it.unisalento.sbapp.Iservice.IuserSelectPostService;
import it.unisalento.sbapp.Repository.UserSelectPostRepository;

@Service
public class UserSelectPostServiceImpl implements IuserSelectPostService{

	@Autowired
	UserSelectPostRepository userSelectPostRepository;
	
	
	@Override
	@Transactional(rollbackOn = SavingUserSelectPostException.class)
	public UserSelectPost save(UserSelectPost userSelectPost) throws SavingUserSelectPostException{
		try {
			return userSelectPostRepository.save(userSelectPost);  //l' id se lo crea da solo il db perchè abbiamo taggato con @Id
			}catch (Exception e) {
				// TODO: handle exception
				throw new SavingUserSelectPostException();
			}
		}
	
	@Override
	@Transactional(rollbackOn = UserSelectPostNotFoundException.class) //rollback mi permette di ritornare al punto prima della transazione quando va male
	public UserSelectPost getById(int id) throws UserSelectPostNotFoundException {
		
														//function arrow
		return userSelectPostRepository.findById(id).orElseThrow(()->new UserSelectPostNotFoundException());
	}
	
	@Override
	@Transactional
	public List<UserSelectPost> getAll() {
		
		return userSelectPostRepository.findAll();
	}
	
/* ATTENZIONE: NON SI POSSONO RICHIAMARE FUNZIONI @Transactional IN ALTRE FUNZIONI @Transactional
 * esempio: save non può essere richiamata in getById
 */
	
	@Override
	@Transactional(rollbackOn = UserSelectPostNotFoundException.class)
	public boolean delete (int id) throws UserSelectPostNotFoundException,IllegalArgumentException{
		
		UserSelectPost userSelectPost = userSelectPostRepository.findById(id).orElseThrow(()-> new UserSelectPostNotFoundException());
		userSelectPostRepository.delete(userSelectPost);
		return true;
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<UserSelectPost> findByUserId(int userId) throws UserNotFoundException {
		try {
			return userSelectPostRepository.findByUserId(userId);
		} catch (Exception e) {
			throw new UserNotFoundException();
		}
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<UserSelectPost> findByUserUsername(String userUsername) throws UserNotFoundException {
		try {
			return userSelectPostRepository.findByUserUsername(userUsername);
		} catch (Exception e) {
			throw new UserNotFoundException();
		}
	}

	@Override
	@Transactional(rollbackOn = PostNotFoundException.class)
	public List<UserSelectPost> findByPostId(int postId) throws PostNotFoundException {
		try {
			return userSelectPostRepository.findByPostId(postId);
		} catch (Exception e) {
			throw new PostNotFoundException();
		}
	}
	
}
