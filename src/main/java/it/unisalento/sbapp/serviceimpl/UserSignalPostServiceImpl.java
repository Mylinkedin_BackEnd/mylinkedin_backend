package it.unisalento.sbapp.ServiceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unisalento.sbapp.DomainClasses.UserSignalPost;
import it.unisalento.sbapp.Exception.SavingUserSignalPostException;
import it.unisalento.sbapp.Exception.UserSignalPostNotFoundException;
import it.unisalento.sbapp.Iservice.IUserSignalPostService;
import it.unisalento.sbapp.Repository.UserSignalPostRepository;


@Service
public class UserSignalPostServiceImpl implements IUserSignalPostService{

	@Autowired
	UserSignalPostRepository userSignalPostRepository;
	
	@Override
	@Transactional(rollbackOn = SavingUserSignalPostException.class)
	public UserSignalPost save(UserSignalPost userSignalPost) throws SavingUserSignalPostException{
		try {
			return userSignalPostRepository.save(userSignalPost);  //l' id se lo crea da solo il db perchè abbiamo taggato con @Id
			}catch (Exception e) {
				// TODO: handle exception
				throw new SavingUserSignalPostException();
			}
		}
	
	@Override
	@Transactional(rollbackOn = UserSignalPostNotFoundException.class) //rollback mi permette di ritornare al punto prima della transazione quando va male
	public UserSignalPost getById(int id) throws UserSignalPostNotFoundException {
		
														//function arrow
		return userSignalPostRepository.findById(id).orElseThrow(()->new UserSignalPostNotFoundException());
	}
	
	@Override
	@Transactional
	public List<UserSignalPost> getAll() {
		
		return userSignalPostRepository.findAll();
	}
	
/* ATTENZIONE: NON SI POSSONO RICHIAMARE FUNZIONI @Transactional IN ALTRE FUNZIONI @Transactional
 * esempio: save non può essere richiamata in getById
 */
	
	@Override
	@Transactional(rollbackOn = UserSignalPostNotFoundException.class)
	public boolean delete (int id) throws UserSignalPostNotFoundException,IllegalArgumentException{
		
		UserSignalPost userSignalPost = userSignalPostRepository.findById(id).orElseThrow(()-> new UserSignalPostNotFoundException());
		userSignalPostRepository.delete(userSignalPost);
		return true;
	}

	@Override
	public List<UserSignalPost> findUserSignaledPostByIdPost(int id_Post) throws UserSignalPostNotFoundException {
		return userSignalPostRepository.findUserSignaledPostByIdPost(id_Post);
	}

	@Override
	public List<UserSignalPost> findPostSignaledByUser(int id_User) throws UserSignalPostNotFoundException {
		return userSignalPostRepository.findPostSignaledByUser(id_User);
	}
	
}
