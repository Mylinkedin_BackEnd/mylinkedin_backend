package it.unisalento.sbapp.ServiceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unisalento.sbapp.DomainClasses.Attached;
import it.unisalento.sbapp.DomainClasses.Post;
import it.unisalento.sbapp.Exception.AttachedNotFoundException;
import it.unisalento.sbapp.Exception.PostNotFoundException;
import it.unisalento.sbapp.Exception.SavingAttachedException;
import it.unisalento.sbapp.Iservice.IAttachedService;
import it.unisalento.sbapp.Repository.AttachedRepository;
import it.unisalento.sbapp.Repository.PostRepository;

@Service
public class AttachedServiceImpl implements IAttachedService{


	@Autowired
	AttachedRepository attachedRepository;
	
	@Autowired
	PostRepository postRepository;
	
	@Override
	@Transactional(rollbackOn = SavingAttachedException.class)
	public Attached save(Attached attached) throws SavingAttachedException{
		
		try {
			return attachedRepository.save(attached);  //l' id se lo crea da solo il db perchè abbiamo taggato con @Id
			}catch (Exception e) {
				// TODO: handle exception
				throw new SavingAttachedException();
			}
		
		}
	
	@Override
	@Transactional(rollbackOn = AttachedNotFoundException.class) //rollback mi permette di ritornare al punto prima della transazione quando va male
	public Attached getById(int id) throws AttachedNotFoundException {
		
														//function arrow
		return attachedRepository.findById(id).orElseThrow(()->new AttachedNotFoundException());
	}
	
	@Override
	@Transactional
	public List<Attached> getAll() {
		
		return attachedRepository.findAll();
	}
	
	@Override
	@Transactional(rollbackOn = AttachedNotFoundException.class)
	public boolean delete (int id) throws AttachedNotFoundException,IllegalArgumentException{
		
		Attached attached = attachedRepository.findById(id).orElseThrow(()-> new AttachedNotFoundException());
		attachedRepository.delete(attached);
		return true;
	}

	@Override
	@Transactional(rollbackOn = PostNotFoundException.class)
	public List<Attached> getByPost(Post post) throws PostNotFoundException {
		try { 
		
			return attachedRepository.findByPost(post);
			}catch (Exception e) {
				// TODO: handle exception
				throw new PostNotFoundException();
			}
		}

	@Override
	public List<Attached> findByPostId(int postId) throws PostNotFoundException {
		Post post =  postRepository.findById(postId).orElseThrow(()-> new PostNotFoundException());
		
		return attachedRepository.findByPost(post);
	}

	
	}
	
