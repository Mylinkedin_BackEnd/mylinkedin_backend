
package it.unisalento.sbapp.ServiceImpl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unisalento.sbapp.DomainClasses.Administrator;
import it.unisalento.sbapp.Exception.SavingUserException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Iservice.IAdministratorService;
import it.unisalento.sbapp.Repository.AdministratorRepository;
import it.unisalento.sbapp.Repository.UserRepository;

@Service
public class AdministratorServiceImpl implements IAdministratorService {

	@Autowired
	AdministratorRepository administratorRepository;

	@Autowired
	UserRepository userRepository;

	@Override
	@Transactional(rollbackOn = SavingUserException.class)
	public Administrator save(Administrator administrator) throws SavingUserException {
		try {
			return administratorRepository.save(administrator); // l' id se lo crea da solo il db perchè abbiamo
																// taggato con @Id
		} catch (Exception e) {
			// TODO: handle exception
			throw new SavingUserException();
		}
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public Administrator getById(int id) throws UserNotFoundException {
		return administratorRepository.findById(id).orElseThrow(() -> new UserNotFoundException());

	}

	@Override
	public List<Administrator> getAll() {
		return administratorRepository.findAll();

	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public boolean delete(int id) throws UserNotFoundException,IllegalArgumentException {
		Administrator administrator = administratorRepository.findById(id)
				.orElseThrow(() -> new UserNotFoundException());
		administratorRepository.delete(administrator);
		return true;
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public Administrator findByUsernameAndPassword(String username, String password) throws UserNotFoundException {
		try {
			return administratorRepository.findByUsernameAndPassword(username, password);
		} catch (Exception e) {
			throw new UserNotFoundException();
		}
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public boolean EnableUserByUsername(String username) throws UserNotFoundException {
		// se action=true --> allora devo mettere una data di abilitazione/accettazione
		// dell'utente

		if (administratorRepository.EnableUser(username, new Date()) == 0)
			throw new UserNotFoundException();
		return true;

	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public boolean EnableUserById(int id_User) throws UserNotFoundException {

		if (administratorRepository.EnableUserById(id_User, new Date()) == 0)
			throw new UserNotFoundException();
		return true;

	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public boolean lockUser(String username ) throws UserNotFoundException {
	
		if (administratorRepository.lockUser(username, new Date()) == 0)
			throw new UserNotFoundException();
		return true;
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public boolean lockUserById(int id ) throws UserNotFoundException {
		
		if (administratorRepository.lockUserById(id, new Date()) == 0)
			throw new UserNotFoundException();
		return true;
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public boolean unlockUser(String username) throws UserNotFoundException {
		if (administratorRepository.unlockUser(username, new Date()) == 0)
			throw new UserNotFoundException();
		return true;
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)

	public boolean unlockUserById(int id) throws UserNotFoundException {
		if (administratorRepository.unlockUserById(id, new Date()) == 0)
			throw new UserNotFoundException();
		return true;
	}

}