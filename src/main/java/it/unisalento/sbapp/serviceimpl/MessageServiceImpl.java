package it.unisalento.sbapp.ServiceImpl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unisalento.sbapp.DomainClasses.Message;
import it.unisalento.sbapp.DomainClasses.User;
import it.unisalento.sbapp.Exception.MessageNotFoundException;
import it.unisalento.sbapp.Exception.SavingMessageException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Iservice.IMessageService;
import it.unisalento.sbapp.Repository.MessageRepository;
import it.unisalento.sbapp.Repository.UserRepository;

@Service
public class MessageServiceImpl implements IMessageService{

	@Autowired
	MessageRepository messageRepository;
	
	@Autowired
	UserRepository userRepository;

	@Override
	@Transactional(rollbackOn = SavingMessageException.class)
	public Message save(Message message) throws SavingMessageException{
		
		try {
			return messageRepository.save(message);  //l' id se lo crea da solo il db perchè abbiamo taggato con @Id
			}catch (Exception e) {
				// TODO: handle exception
				throw new SavingMessageException();
			}
		
		}
	
	@Override
	@Transactional(rollbackOn = MessageNotFoundException.class) //rollback mi permette di ritornare al punto prima della transazione quando va male
	public Message getById(int id) throws MessageNotFoundException {
		
														//function arrow
		return messageRepository.findById(id).orElseThrow(()->new MessageNotFoundException());
	}
	
	@Override
	@Transactional
	public List<Message> getAll() {
		
		return messageRepository.findAll();
	}
	
	@Override
	@Transactional(rollbackOn = MessageNotFoundException.class)
	public boolean delete (int id) throws MessageNotFoundException, IllegalArgumentException{
		
		Message message = messageRepository.findById(id).orElseThrow(()-> new MessageNotFoundException());
		messageRepository.delete(message);
		return true;
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<Message> findByUserSender(User userSender) throws UserNotFoundException {
		try {
			return messageRepository.findByUserSender(userSender);
		} catch (Exception e) {
			throw new UserNotFoundException();
		}
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<Message> findByUserSenderId(int userSender_id) throws UserNotFoundException {
		User user =  userRepository.findById(userSender_id).orElseThrow(()-> new UserNotFoundException());
		return messageRepository.findByUserSender(user);
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<Message> findByUserReceiver(User userReceiver) throws UserNotFoundException {
		try {
			return messageRepository.findByUserReceiver(userReceiver);
		} catch (Exception e) {
			throw new UserNotFoundException();
		}
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<Message> findByUserReceiverId(int userReceiver_id) throws UserNotFoundException {
		User user =  userRepository.findById(userReceiver_id).orElseThrow(()-> new UserNotFoundException());
		return messageRepository.findByUserReceiver(user);
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<Message> findByUserReceiverIdAndMessageRead(int userReceiver_id, boolean messageRead) throws UserNotFoundException {
		User user =  userRepository.findById(userReceiver_id).orElseThrow(()-> new UserNotFoundException());
		List<Message> userMessage = messageRepository.findByUserReceiver(user);
		
		List<Message> unreadMessages = new ArrayList<Message>();
		
		for (Message message : userMessage) {
			if(message.isMessage_Read()== false) {
				unreadMessages.add(message);
				}
			}
		return unreadMessages;
		
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<Message> findByUserReceiverIdAndUserSenderId(int userReceiver_id, int userSender_id)throws UserNotFoundException{
		try {
			return messageRepository.findByUserReceiverIdAndUserSenderId(userReceiver_id, userSender_id);

		} catch (Exception e) {
			throw new UserNotFoundException();
		}
		
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<Message> findByUserReceiverUsernameLikeAndUserSenderUsernameLike(String userReceiverUsername,
			String userSenderUsername) throws UserNotFoundException{
		try {
			return messageRepository.findByUserReceiverUsernameLikeAndUserSenderUsernameLike(userReceiverUsername, userSenderUsername);

		} catch (Exception e) {
			throw new UserNotFoundException();
		}
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<Message> findByUserReceiverUsernameAndUserSenderId(String receiverUsername, int senderId)
			throws UserNotFoundException {
		try {
			return messageRepository.findByUserReceiverUsernameAndUserSenderId(receiverUsername, senderId);
		} catch (Exception e) {
			throw new UserNotFoundException();
		}
	}


}
