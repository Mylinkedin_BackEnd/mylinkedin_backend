package it.unisalento.sbapp.ServiceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unisalento.sbapp.DomainClasses.Structure;
import it.unisalento.sbapp.Exception.SavingStructureException;
import it.unisalento.sbapp.Exception.StructureNotFoundException;
import it.unisalento.sbapp.Iservice.IStructureService;
import it.unisalento.sbapp.Repository.StructureRepository;

@Service
public class StructureServiceImpl implements IStructureService{
	

	@Autowired
	StructureRepository structureRepository;

	@Override
	@Transactional(rollbackOn = SavingStructureException.class)
	public Structure save(Structure structure) throws SavingStructureException{
		
		try {
			return structureRepository.save(structure);  //l' id se lo crea da solo il db perchè abbiamo taggato con @Id
			}catch (Exception e) {
				// TODO: handle exception
				throw new SavingStructureException();
			}
		
		}
	
	@Override
	@Transactional(rollbackOn = StructureNotFoundException.class) //rollback mi permette di ritornare al punto prima della transazione quando va male
	public Structure getById(int id) throws StructureNotFoundException {
		
														//function arrow
		return structureRepository.findById(id).orElseThrow(()->new StructureNotFoundException());
	}
	
	@Override
	@Transactional
	public List<Structure> getAll() {
		
		return structureRepository.findAll();
	}
	
	@Override
	@Transactional(rollbackOn = StructureNotFoundException.class)
	public boolean delete (int id) throws StructureNotFoundException,IllegalArgumentException{
		
		Structure structure = structureRepository.findById(id).orElseThrow(()-> new StructureNotFoundException());
		structureRepository.delete(structure);
		return true;
	}

	@Override
	@Transactional(rollbackOn = StructureNotFoundException.class)
	public Structure findByTitle(String title) throws StructureNotFoundException {
		try {
			return structureRepository.findByTitle(title);
		} catch (Exception e) {
			throw new StructureNotFoundException();
		}
	}

}
