package it.unisalento.sbapp.ServiceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unisalento.sbapp.DomainClasses.UserViewPost;
import it.unisalento.sbapp.Exception.PostNotFoundException;
import it.unisalento.sbapp.Exception.SavingUserViewPostException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Exception.UserViewPostNotFoundException;
import it.unisalento.sbapp.Iservice.IUserViewPostService;
import it.unisalento.sbapp.Repository.UserViewPostRepository;

@Service
public class UserVIewPostServiceImpl implements IUserViewPostService{

	@Autowired
	UserViewPostRepository userViewPostRepository;
	
	
	@Override
	@Transactional(rollbackOn = SavingUserViewPostException.class)
	public UserViewPost save(UserViewPost userViewPost) throws SavingUserViewPostException{
		try {
			return userViewPostRepository.save(userViewPost);  //l' id se lo crea da solo il db perchè abbiamo taggato con @Id
			}catch (Exception e) {
				// TODO: handle exception
				throw new SavingUserViewPostException();
			}
		}
	
	@Override
	@Transactional(rollbackOn = UserViewPostNotFoundException.class) //rollback mi permette di ritornare al punto prima della transazione quando va male
	public UserViewPost getById(int id) throws UserViewPostNotFoundException {
		
														//function arrow
		return userViewPostRepository.findById(id).orElseThrow(()->new UserViewPostNotFoundException());
	}
	
	@Override
	@Transactional
	public List<UserViewPost> getAll() {
		
		return userViewPostRepository.findAll();
	}
	
/* ATTENZIONE: NON SI POSSONO RICHIAMARE FUNZIONI @Transactional IN ALTRE FUNZIONI @Transactional
 * esempio: save non può essere richiamata in getById
 */
	
	@Override
	@Transactional(rollbackOn = UserViewPostNotFoundException.class)
	public boolean delete (int id) throws UserViewPostNotFoundException,IllegalArgumentException{
		
		UserViewPost userViewPost = userViewPostRepository.findById(id).orElseThrow(()-> new UserViewPostNotFoundException());
		userViewPostRepository.delete(userViewPost);
		return true;
		
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<UserViewPost> findByPostId(int postId) throws UserViewPostNotFoundException {
		
		try {
			return userViewPostRepository.findByPostId(postId);
		} catch (Exception e) {
			throw new UserViewPostNotFoundException();
		}
	}

	@Override
	@Transactional(rollbackOn = PostNotFoundException.class)

	
	public List<UserViewPost> findByUserId(int userId) throws UserViewPostNotFoundException {
		try {
			return userViewPostRepository.findByUserId(userId);
		} catch (Exception e) {
			throw new UserViewPostNotFoundException();
		}
	}
	
}
