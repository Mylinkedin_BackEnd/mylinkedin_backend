package it.unisalento.sbapp.ServiceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unisalento.sbapp.DomainClasses.Applicant;
import it.unisalento.sbapp.Exception.SavingUserException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Iservice.IApplicantService;
import it.unisalento.sbapp.Repository.ApplicantRepository;

@Service
public class ApplicantServiceImpl implements IApplicantService{

	@Autowired
	ApplicantRepository applicantRepository;
	
	@Override
	@Transactional(rollbackOn = SavingUserException.class)
	public Applicant save(Applicant applicant) throws SavingUserException {
		try {
			return applicantRepository.save(applicant);  //l' id se lo crea da solo il db perchè abbiamo taggato con @Id
			}catch (Exception e) {
				// TODO: handle exception
				throw new SavingUserException();
			}
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public Applicant getById(int id) throws UserNotFoundException {
		return applicantRepository.findById(id).orElseThrow(()->new UserNotFoundException());

	}

	@Override
	public List<Applicant> getAll() {
		return applicantRepository.findAll();

	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public boolean delete(int id) throws UserNotFoundException,IllegalArgumentException {
		Applicant applicant = applicantRepository.findById(id).orElseThrow(()-> new UserNotFoundException());
		applicantRepository.delete(applicant);	
		return true;
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<Applicant> getByDisable(boolean disable) throws UserNotFoundException {
		try {
			return applicantRepository.findByDisable(disable);
		} catch (Exception e) {
			throw new UserNotFoundException();
		}
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<Applicant> findByCompanyId(int companyId) throws UserNotFoundException {
		try {
			return applicantRepository.findByCompanyId(companyId);
		} catch (Exception e) {
			throw new UserNotFoundException();
		}
	}
	
	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public Applicant findByUsernameAndPassword(String username, String password) throws UserNotFoundException {
		try {
			return applicantRepository.findByUsernameAndPassword(username, password);
		} catch (Exception e) {
			throw new UserNotFoundException();
		}
	}

}
