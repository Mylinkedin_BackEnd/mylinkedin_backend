package it.unisalento.sbapp.ServiceImpl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unisalento.sbapp.DomainClasses.Post;
import it.unisalento.sbapp.DomainClasses.User;
import it.unisalento.sbapp.Exception.PostNotFoundException;
import it.unisalento.sbapp.Exception.SavingPostException;
import it.unisalento.sbapp.Exception.StructureNotFoundException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Iservice.IPostService;
import it.unisalento.sbapp.Repository.PostRepository;
import it.unisalento.sbapp.Repository.UserRepository;

@Service
public class PostServiceImpl implements IPostService{

	@Autowired
	PostRepository postRepository;
	
	@Autowired
	UserRepository userRepository;

	@Override
	@Transactional(rollbackOn = SavingPostException.class)
	public Post save(Post post) throws SavingPostException{
		
		try {
			return postRepository.save(post);  //l' id se lo crea da solo il db perchè abbiamo taggato con @Id
			}catch (Exception e) {
				// TODO: handle exception
				throw new SavingPostException();
			}
		
		}
	
	@Override
	@Transactional(rollbackOn = PostNotFoundException.class) //rollback mi permette di ritornare al punto prima della transazione quando va male
	public Post getById(int id) throws PostNotFoundException {
														//function arrow
		return postRepository.findById(id).orElseThrow(()->new PostNotFoundException());
	}
	
	@Override
	@Transactional
	public List<Post> getAll() {
		
		return postRepository.findAll();
	}
	
	@Override
	@Transactional(rollbackOn = PostNotFoundException.class)
	public boolean deleteById (int id) throws PostNotFoundException,IllegalArgumentException{
		
		Post post = postRepository.findById(id).orElseThrow(()-> new PostNotFoundException());
		postRepository.delete(post);
		return true;
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<Post> findByUserName(String userName) throws UserNotFoundException {
		// TODO Auto-generated method stub
		try {
			User user =  userRepository.findByUsernameLike(userName);
			return postRepository.findByUser(user);
			
		} catch (Exception e) {
			throw new UserNotFoundException();
		}

	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<Post> findByUserId(int userId) throws UserNotFoundException{
		// TODO Auto-generated method stub
		User user =  userRepository.findById(userId).orElseThrow(()-> new UserNotFoundException());
		
		return postRepository.findByUser(user);
	}
	
	@Transactional(rollbackOn = PostNotFoundException.class)
	@Override
	public boolean delete (Post post) throws IllegalArgumentException{
		
			postRepository.delete(post);
			return true;
		
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<Post> findByUser(User user) throws UserNotFoundException {
		// TODO Auto-generated method stub
		try { 
			return postRepository.findByUser(user);
			}catch (Exception e) {
				// TODO: handle exception
				throw new UserNotFoundException();
			}
		}

	@Override
	@Transactional(rollbackOn = PostNotFoundException.class)
	public List<Post> findByPubblicationDateAfter(Date pubblicationDate) throws PostNotFoundException {
		try { 
			return postRepository.findByPubblicationDateAfter(pubblicationDate);
			}catch (Exception e) {
				// TODO: handle exception
				throw new PostNotFoundException();
			}
	}

	@Override
	@Transactional(rollbackOn = StructureNotFoundException.class)
	public List<Post> findByPubblicationDateAfterAndStructureId(Date pubblicationDate, int structureId)
			throws StructureNotFoundException {
		try { 
			return postRepository.findByPubblicationDateAfter(pubblicationDate);
			}catch (Exception e) {
				// TODO: handle exception
				throw new StructureNotFoundException();
			}
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<Post> findByPubblicationDateAfterAndUserId(Date pubblicationDate, int userId)
			throws UserNotFoundException {
		try { 
			return postRepository.findByPubblicationDateAfterAndUserId(pubblicationDate,userId);
			}catch (Exception e) {
				// TODO: handle exception
				throw new UserNotFoundException();
			}
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class )
	public List<Post> findByPubblicationDateAfterAndStructureIdAndUserId(Date pubblicationDate, int structureId,
			int userId) throws UserNotFoundException, StructureNotFoundException {
		try { 
			return postRepository.findByPubblicationDateAfterAndStructureIdAndUserId(pubblicationDate,structureId,userId);
			}catch (Exception e) {
				// TODO: handle exception
				throw new UserNotFoundException();
			}
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class )
	public List<Post> findByStructureIdAndUserId(int structureId, int userId)
			throws UserNotFoundException, StructureNotFoundException {
		try { 
			return postRepository.findByStructureIdAndUserId(structureId,userId);
			}catch (Exception e) {
				// TODO: handle exception
				throw new UserNotFoundException();
			}
	}

	@Override
	@Transactional(rollbackOn = PostNotFoundException.class)
	public void ModifyPostWithQuery(String title, int id) throws PostNotFoundException {
		try {
			postRepository.ModifyPostWithQuery(title, id);
 
		} catch (Exception e) {
			throw new PostNotFoundException();
		}
	}

	}

