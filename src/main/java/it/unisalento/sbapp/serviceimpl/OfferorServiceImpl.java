package it.unisalento.sbapp.ServiceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unisalento.sbapp.DomainClasses.Offeror;
import it.unisalento.sbapp.Exception.SavingUserException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Iservice.IOfferorService;
import it.unisalento.sbapp.Repository.OfferorRepository;

@Service
public class OfferorServiceImpl implements IOfferorService{

	@Autowired
	OfferorRepository offerorRepository;
	
	@Override
	@Transactional(rollbackOn = SavingUserException.class)
	public Offeror save(Offeror offeror) throws SavingUserException {
		try {
			return offerorRepository.save(offeror);  //l' id se lo crea da solo il db perchè abbiamo taggato con @Id
			}catch (Exception e) {
				// TODO: handle exception
				throw new SavingUserException();
			}
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class) //rollback mi permette di ritornare al punto prima della transazione quando va male
	public Offeror getById(int id) throws UserNotFoundException {
		
		return offerorRepository.findById(id).orElseThrow(()->new UserNotFoundException());
	}

	@Override
	public List<Offeror> getAll() {
		
		return offerorRepository.findAll();
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class) //rollback mi permette di ritornare al punto prima della transazione quando va male
	public boolean delete(int id) throws UserNotFoundException,IllegalArgumentException {
		Offeror offeror = offerorRepository.findById(id).orElseThrow(()-> new UserNotFoundException());
		offerorRepository.delete(offeror);	
		return true;
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<Offeror> findByDisable(boolean disable) throws UserNotFoundException {
		try {
			return offerorRepository.findByDisable(disable);
		} catch (Exception e) {
			throw new UserNotFoundException();
		}
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public List<Offeror> findByCompanyId( int companyId) throws UserNotFoundException {
		try {
			return offerorRepository.findByCompanyId(companyId);
		} catch (Exception e) {
			throw new UserNotFoundException();
		}
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public Offeror findByUsernameAndPassword(String username, String password) throws UserNotFoundException {
		try {
			return offerorRepository.findByUsernameAndPassword(username, password);
		} catch (Exception e) {
			throw new UserNotFoundException();
		}
	}
}
