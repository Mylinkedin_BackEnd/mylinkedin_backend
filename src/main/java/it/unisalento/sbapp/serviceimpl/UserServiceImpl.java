package it.unisalento.sbapp.ServiceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unisalento.sbapp.DomainClasses.User;
import it.unisalento.sbapp.Exception.SavingUserException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Iservice.IUserService;
import it.unisalento.sbapp.Repository.UserRepository;

@Service
public class UserServiceImpl implements IUserService{

	@Autowired
	UserRepository userRepository;
	
	@Override
	@Transactional(rollbackOn = SavingUserException.class)
	public User save(User user) throws SavingUserException{
		try {
			return userRepository.save(user);  //l' id se lo crea da solo il db perchè abbiamo taggato con @Id
			}catch (Exception e) {
				// TODO: handle exception
				throw new SavingUserException();
			}
		}
	
	@Override
	@Transactional(rollbackOn = UserNotFoundException.class) //rollback mi permette di ritornare al punto prima della transazione quando va male
	public User getById(int id) throws UserNotFoundException {
		
														//function arrow
		return userRepository.findById(id).orElseThrow(()->new UserNotFoundException());
	}
	
	@Override
	@Transactional
	public List<User> getAll() {
		
		return userRepository.findAll();
	}
	
/* ATTENZIONE: NON SI POSSONO RICHIAMARE FUNZIONI @Transactional IN ALTRE FUNZIONI @Transactional
 * esempio: save non può essere richiamata in getById
 */
	
	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public boolean delete (int id) throws UserNotFoundException,IllegalArgumentException{
		
		User user = userRepository.findById(id).orElseThrow(()-> new UserNotFoundException());
		userRepository.delete(user);
		return true;
	}

	@Override
	@Transactional
	public List<User> getByName(String name) throws UserNotFoundException {
		try {
			return userRepository.findByNameLike(name);

		} catch (Exception e) {
			throw new UserNotFoundException();
		}
	}

	@Override
	@Transactional
	public List<User> getBySurname(String surname) throws UserNotFoundException {
		try {
			return userRepository.findBySurnameLike(surname);

		} catch (Exception e) {
			throw new UserNotFoundException();

		}
	}

	@Override
	@Transactional
	public List<User> getByNameAndSurname(String name, String surname) throws UserNotFoundException {
		try {
			return userRepository.findByNameAndSurnameLike(name, surname);

		} catch (Exception e) {
			throw new UserNotFoundException();
		}
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public User getByUsernameAndPassword(String username, String password) throws UserNotFoundException {
		// TODO Auto-generated method stub
		try {
			return userRepository.findByUsernameAndPasswordLike(username, password);

		} catch (Exception e) {
			throw new UserNotFoundException();
			}
		
	}
	
	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public User getByEmailAndPassword(String email, String password) throws UserNotFoundException {
		// TODO Auto-generated method stub
		try {
			return userRepository.findByEmailAndPasswordLike(email, password);

		} catch (Exception e) {
			throw new UserNotFoundException();
			}
		
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public User getByEmailOrUsernameLike(String email, String username) throws UserNotFoundException {
		try {
			return userRepository.findByEmailOrUsernameLike(email, username);

		} catch (Exception e) {
			throw new UserNotFoundException();
			}
	}


	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public User getByUsernameLike(String username) throws UserNotFoundException {
		try {
			return userRepository.findByUsernameLike(username);

		} catch (Exception e) {
			throw new UserNotFoundException();
		}
		
	}
	
	
		@Override
		@Transactional(rollbackOn = UserNotFoundException.class)
		public boolean modifyPhoneNumber(String phoneNumber, int id) throws UserNotFoundException {
			try {
				if(userRepository.ModifyPhoneNumberWithQuery(phoneNumber, id)==0)
					throw new UserNotFoundException();
				return true;
	 
			} catch (Exception e) {
				throw new UserNotFoundException();
			}
		}
		
		@Override
		@Transactional(rollbackOn = UserNotFoundException.class)
		public boolean modifyAddress(String address, int id) throws UserNotFoundException {
			try {
				if(userRepository.ModifyAddressWithQuery(address, id)==0)throw new UserNotFoundException();
				return true;
	 
			} catch (Exception e) {
				throw new UserNotFoundException();
			}
		}
		
		@Override
		@Transactional(rollbackOn = UserNotFoundException.class)
		public boolean modifyCity(String city, int id) throws UserNotFoundException {
			try {
				if(userRepository.ModifyCityWithQuery(city, id)==0)throw new UserNotFoundException();
				return true;
			} catch (Exception e) {
				throw new UserNotFoundException();
			}
		}
	}
	

