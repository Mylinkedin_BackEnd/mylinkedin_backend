package it.unisalento.sbapp.ServiceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unisalento.sbapp.DomainClasses.Attribute;
import it.unisalento.sbapp.DomainClasses.AttributeStructure;
import it.unisalento.sbapp.DomainClasses.Structure;
import it.unisalento.sbapp.Exception.AttributeStructureNotFound;
import it.unisalento.sbapp.Exception.SavingAttributeStructureException;
import it.unisalento.sbapp.Iservice.IAttributeStructureService;
import it.unisalento.sbapp.Repository.AttributeStructureRepository;


@Service
public class AttributeStructureServiceImpl implements IAttributeStructureService{


	@Autowired
	AttributeStructureRepository attributeStructureRepository;
	
	@Override
	@Transactional(rollbackOn = SavingAttributeStructureException.class)
	public AttributeStructure save(AttributeStructure attributeStructure) throws SavingAttributeStructureException{
		
		try {
			return attributeStructureRepository.save(attributeStructure);  //l' id se lo crea da solo il db perchè abbiamo taggato con @Id
			}catch (Exception e) {
				// TODO: handle exception
				throw new SavingAttributeStructureException();
			}
		
		}
	
	@Override
	@Transactional(rollbackOn = AttributeStructureNotFound.class) //rollback mi permette di ritornare al punto prima della transazione quando va male
	public AttributeStructure getById(int id) throws AttributeStructureNotFound {
		
														//function arrow
		return attributeStructureRepository.findById(id).orElseThrow(()->new AttributeStructureNotFound());
	}
	
	@Override
	@Transactional
	public List<AttributeStructure> getAll() {
		
		return attributeStructureRepository.findAll();
	}
	
	@Override
	@Transactional(rollbackOn = AttributeStructureNotFound.class)
	public boolean delete (int id) throws AttributeStructureNotFound,IllegalArgumentException{
		
		AttributeStructure attributeStructure = attributeStructureRepository.findById(id).orElseThrow(()-> new AttributeStructureNotFound());
		attributeStructureRepository.delete(attributeStructure);
		return true;
	}

	@Override
	@Transactional(rollbackOn = AttributeStructureNotFound.class)
	public List<AttributeStructure> findByStructureId(int structureId) throws AttributeStructureNotFound {
		try {
			return attributeStructureRepository.findByStructureId(structureId);

		} catch (Exception e) {
			throw new AttributeStructureNotFound();
		}
		
	}

	@Override
	@Transactional(rollbackOn = AttributeStructureNotFound.class)
	public List<AttributeStructure> findByStructureTitle(String structureTitle) throws AttributeStructureNotFound {
		try {
			return attributeStructureRepository.findByStructureTitle(structureTitle);

		} catch (Exception e) {
			throw new AttributeStructureNotFound();
		}
	}
	
	@Override
	@Transactional(rollbackOn = IllegalArgumentException.class)
	public void createBound(Structure structure, Attribute relatedAttribute) throws IllegalArgumentException{
		AttributeStructure bound=new AttributeStructure();
		bound.setAttribute(relatedAttribute);
		bound.setStructure(structure);
		attributeStructureRepository.save(bound);
		
	}



	
	
}