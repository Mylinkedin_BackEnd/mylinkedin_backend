package it.unisalento.sbapp.ServiceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unisalento.sbapp.DomainClasses.Comment;
import it.unisalento.sbapp.DomainClasses.Post;
import it.unisalento.sbapp.Exception.CommentNotFoundException;
import it.unisalento.sbapp.Exception.PostNotFoundException;
import it.unisalento.sbapp.Exception.SavingCommentException;
import it.unisalento.sbapp.Iservice.ICommentService;
import it.unisalento.sbapp.Repository.CommentRepository;
import it.unisalento.sbapp.Repository.PostRepository;

@Service
public class CommentServiceImpl implements ICommentService{


	@Autowired
	CommentRepository commentRepository;
	
	@Autowired
	PostRepository postRepository;

	@Override
	@Transactional(rollbackOn = SavingCommentException.class)
	public Comment save(Comment comment) throws SavingCommentException{
		
		try {
			return commentRepository.save(comment);  //l' id se lo crea da solo il db perchè abbiamo taggato con @Id
			}catch (Exception e) {
				// TODO: handle exception
				throw new SavingCommentException();
			}
		
		}
	
	@Override
	@Transactional(rollbackOn = CommentNotFoundException.class) //rollback mi permette di ritornare al punto prima della transazione quando va male
	public Comment getById(int id) throws CommentNotFoundException {
		
														//function arrow
		return commentRepository.findById(id).orElseThrow(()->new CommentNotFoundException());
	}
	
	@Override
	@Transactional
	public List<Comment> getAll() {
		
		return commentRepository.findAll();
	}
	
	@Override
	@Transactional(rollbackOn = CommentNotFoundException.class)
	public boolean delete (int id) throws CommentNotFoundException, IllegalArgumentException{
		
		Comment comment = commentRepository.findById(id).orElseThrow(()-> new CommentNotFoundException());
		commentRepository.delete(comment);
		return true;
	}

	@Override
	@Transactional(rollbackOn = PostNotFoundException.class)
	public List<Comment> getByPostId(int postId) throws PostNotFoundException {
		try {
			
			Post post =  postRepository.findById(postId).orElseThrow(()-> new PostNotFoundException());
			
			return commentRepository.findByPost(post);
			
		} catch (Exception e) {
			throw new PostNotFoundException();
		}
	}

	@Override
	@Transactional(rollbackOn = PostNotFoundException.class)
	public List<Comment> getByPost(Post post) throws PostNotFoundException {
		try {
			
			return commentRepository.findByPost(post);
			
		} catch (Exception e) {
			throw new PostNotFoundException();
		}
		
	}

	@Override
	@Transactional(rollbackOn = CommentNotFoundException.class)
	public List<Comment> getByCommentId(int commentId) throws CommentNotFoundException {
	try {
			
			Comment comment =  commentRepository.findById(commentId).orElseThrow(()-> new CommentNotFoundException());
			
			return commentRepository.findByComment(comment);
			
		} catch (Exception e) {
			throw new CommentNotFoundException();
		}
	}

	@Override
	public List<Comment> getByComment(Comment comment) throws CommentNotFoundException {
		try {
			return commentRepository.findByComment(comment);
		} catch (Exception e) {
			throw new CommentNotFoundException();
		}
	}
	
}
