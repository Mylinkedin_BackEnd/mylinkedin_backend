package it.unisalento.sbapp.ServiceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unisalento.sbapp.DomainClasses.ProfileImage;
import it.unisalento.sbapp.DomainClasses.User;
import it.unisalento.sbapp.Exception.ProfileImageNotFoundException;
import it.unisalento.sbapp.Exception.SavingProfileImageException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Iservice.IProfileImageService;
import it.unisalento.sbapp.Repository.ProfileImageRepository;
import it.unisalento.sbapp.Repository.UserRepository;

@Service
public class ProfileImageServiceImpl implements IProfileImageService{

	@Autowired
	ProfileImageRepository profileImageRepository;
	
	@Autowired
	UserRepository userRepository;

	@Override
	@Transactional(rollbackOn = SavingProfileImageException.class)
	public ProfileImage save(ProfileImage profileImage) throws SavingProfileImageException{
		
		try {
			return profileImageRepository.save(profileImage);  //l' id se lo crea da solo il db perchè abbiamo taggato con @Id
			}catch (Exception e) {
				// TODO: handle exception
				throw new SavingProfileImageException();
			}
		
		}
	
	@Override
	@Transactional(rollbackOn = ProfileImageNotFoundException.class) //rollback mi permette di ritornare al punto prima della transazione quando va male
	public ProfileImage getById(int id) throws ProfileImageNotFoundException {
		
														//function arrow
		return profileImageRepository.findById(id).orElseThrow(()->new ProfileImageNotFoundException());
	}
	
	@Override
	@Transactional
	public List<ProfileImage> getAll() {
		
		return profileImageRepository.findAll();
	}
	
	@Override
	@Transactional(rollbackOn = ProfileImageNotFoundException.class)
	public boolean delete (int id) throws ProfileImageNotFoundException,IllegalArgumentException{
		
		ProfileImage profileImage = profileImageRepository.findById(id).orElseThrow(()-> new ProfileImageNotFoundException());
		profileImageRepository.delete(profileImage);
		return true;
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public ProfileImage findByUser(User user) throws UserNotFoundException {
		try {
			return profileImageRepository.findByUser(user);
		} catch (Exception e) {
			throw new UserNotFoundException();
		}
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public ProfileImage findByUserId(int userId) throws UserNotFoundException {
		
		User user = userRepository.findById(userId).orElseThrow(()->new UserNotFoundException());
		return profileImageRepository.findByUser(user);
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public ProfileImage findByUserUsername(String userUsername) throws UserNotFoundException {
		try {
			User user = userRepository.findByUsernameLike(userUsername);
			return profileImageRepository.findByUser(user);
			
		} catch (Exception e) {
			throw new UserNotFoundException();
		}

	}
}
