package it.unisalento.sbapp.ServiceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unisalento.sbapp.DomainClasses.Attribute;
import it.unisalento.sbapp.Exception.AttributeNotFoundException;
import it.unisalento.sbapp.Exception.SavingAttributeException;
import it.unisalento.sbapp.Iservice.IAttributeService;
import it.unisalento.sbapp.Repository.AttributeRepository;

@Service
public class AttributeServiceImpl implements IAttributeService{


	@Autowired
	AttributeRepository attributeRepository;

	@Override
	@Transactional(rollbackOn = SavingAttributeException.class)
	public Attribute save(Attribute attribute) throws SavingAttributeException{
		
		try {
			return attributeRepository.save(attribute);  //l' id se lo crea da solo il db perchè abbiamo taggato con @Id
			}catch (Exception e) {
				// TODO: handle exception
				throw new SavingAttributeException();
			}
		
		}
	
	@Override
	@Transactional(rollbackOn = AttributeNotFoundException.class) //rollback mi permette di ritornare al punto prima della transazione quando va male
	public Attribute getById(int id) throws AttributeNotFoundException {
		
														//function arrow
		return attributeRepository.findById(id).orElseThrow(()->new AttributeNotFoundException());
	}
	
	@Override
	@Transactional
	public List<Attribute> getAll() {
		
		return attributeRepository.findAll();
	}
	
	@Override
	@Transactional(rollbackOn = AttributeNotFoundException.class)
	public boolean delete (int id) throws AttributeNotFoundException, IllegalArgumentException{
		
		Attribute attribute = attributeRepository.findById(id).orElseThrow(()-> new AttributeNotFoundException());
		attributeRepository.delete(attribute);
		return true;
	}

	@Override
	@Transactional(rollbackOn = AttributeNotFoundException.class)
	public Attribute getByNameLike(String name) throws AttributeNotFoundException {
		try {
			return attributeRepository.findByNameLike(name);
		} catch (Exception e) {
			throw new AttributeNotFoundException();
		}
	}

	
}
