package it.unisalento.sbapp.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "C'è stato qualche errore: Azienda non aggiunto")
public class SavingCompanyException extends Exception{

}
