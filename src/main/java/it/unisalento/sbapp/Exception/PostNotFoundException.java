package it.unisalento.sbapp.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Post non trovato") // come impostare il code di response HTTP
public class PostNotFoundException extends Exception{

}
