package it.unisalento.sbapp.DTOClasses;

import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import it.unisalento.sbapp.MyInterface.IUserDTO;
import it.unisalento.sbapp.validators.CheckEmail;
import it.unisalento.sbapp.validators.CheckName;
import it.unisalento.sbapp.validators.MatchFieldsConstraint;

public class UserDTO implements IUserDTO {

	int id;

	@NotBlank
	String name;

	String surname;

	// @NotBlank
	// @NotNull
	String username;
	// @NotBlank
	// @NotNull
	String password;
	String address;
	String city;
	//@NotBlank
//	@Email
	String email;
	Integer CAP;
//	@NotBlank
//	@Email
//	String emailToVerify;
	String nation;
	String phoneNumber;

	// @PastOrPresent
	Date registrationDate;
	Date bornDate;

	String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getCAP() {
		return CAP;
	}

	public void setCAP(Integer cAP) {
		CAP = cAP;
	}

	public Date getBornDate() {
		return bornDate;
	}

	public void setBornDate(Date bornDate) {
		this.bornDate = bornDate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
