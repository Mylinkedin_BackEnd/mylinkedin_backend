package it.unisalento.sbapp.DTOClasses;

import java.util.Date;

public class OfferorDTO extends UserDTO {

	boolean disable;
	Date abilitationDate;
	Date blockingStartDate;
	Date blockingEndDate;
	Integer companyId;
	String recruimentSector;
	
	
	public Date getAbilitationDate() {
		return abilitationDate;
	}

	public void setAbilitationDate(Date abilitationDate) {
		this.abilitationDate = abilitationDate;
	}

	public String getRecruimentSector() {
		return recruimentSector;
	}

	public void setRecruimentSector(String recruimentSector) {
		this.recruimentSector = recruimentSector;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public OfferorDTO() {}

	public boolean isDisable() {
		return disable;
	}

	public void setDisable(boolean disable) {
		this.disable = disable;
	}

	public Date getBlockingStartDate() {
		return blockingStartDate;
	}

	public void setBlockingStartDate(Date blockingStartDate) {
		this.blockingStartDate = blockingStartDate;
	}

	public Date getBlockingEndDate() {
		return blockingEndDate;
	}

	public void setBlockingEndDate(Date blockingEndDate) {
		this.blockingEndDate = blockingEndDate;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	@Override
	public String toString() {
		return "OfferorDTO [disable=" + disable + ", abilitationDate=" + abilitationDate + ", blockingStartDate="
				+ blockingStartDate + ", blockingEndDate=" + blockingEndDate + ", companyId=" + companyId
				+ ", recruimentSector=" + recruimentSector + ", id=" + id + ", name=" + name + ", surname=" + surname
				+ ", username=" + username + ", password=" + password + ", address=" + address + ", city=" + city
				+ ", email=" + email + ", CAP=" + CAP + ", nation=" + nation + ", phoneNumber=" + phoneNumber
				+ ", registrationDate=" + registrationDate + ", bornDate=" + bornDate + ", type=" + type + "]";
	}

	
	
}
