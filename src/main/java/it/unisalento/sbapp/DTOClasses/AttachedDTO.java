package it.unisalento.sbapp.DTOClasses;


public class AttachedDTO {


	Integer id;
	
//	@NotBlank
//	String name;
//	String format;
	byte [] file;
	
	Integer post_id;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	
	public byte[] getFile() {
		return file;
	}
	public void setFile(byte[] file) {
		this.file = file;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setPost_id(Integer post_id) {
		this.post_id = post_id;
	}
	public Integer getPost_id() {
		return post_id;
	}
	public void setPost_id(int post_id) {
		this.post_id = post_id;
	}
	
	
}
