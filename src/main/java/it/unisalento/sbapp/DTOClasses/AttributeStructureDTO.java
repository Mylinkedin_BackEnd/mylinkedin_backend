package it.unisalento.sbapp.DTOClasses;

import javax.validation.constraints.NotNull;

public class AttributeStructureDTO {
	
	int id;
	
	@NotNull
	Integer attribute_id;
	@NotNull
	Integer structure_id;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Integer getAttribute_id() {
		return attribute_id;
	}
	public void setAttribute_id(int attribute_id) {
		this.attribute_id = attribute_id;
	}
	public Integer getStructure_id() {
		return structure_id;
	}
	public void setStructure_id(int structure_id) {
		this.structure_id = structure_id;
	}


	
	
}
