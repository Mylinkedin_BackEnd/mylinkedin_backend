package it.unisalento.sbapp.DTOClasses;


import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

public class MessageDTO {

	
	int id;
	
	@NotBlank
	@NotNull
	String text;
	
	@PastOrPresent
	Date sentDate;
	boolean messageRead;

	Integer user_receiver_id;
	Integer user_sender_id;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getSentDate() {
		return sentDate;
	}

	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}

	public Integer getUser_receiver_id() {
		return user_receiver_id;
	}

	public void setUser_receiver_id(int user_receiver_id) {
		this.user_receiver_id = user_receiver_id;
	}

	public Integer getUser_sender_id() {
		return user_sender_id;
	}

	public void setUser_sender_id(int user_sender_id) {
		this.user_sender_id = user_sender_id;
	}

	public boolean isRead() {
		return messageRead;
	}

	public void setRead(boolean read) {
		this.messageRead = read;
	}
	
	
}
