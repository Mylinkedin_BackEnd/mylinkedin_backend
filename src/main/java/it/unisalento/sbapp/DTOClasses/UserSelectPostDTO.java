package it.unisalento.sbapp.DTOClasses;

import javax.validation.constraints.NotNull;

public class UserSelectPostDTO {

	Integer id;
	@NotNull
	Integer user_id;
	@NotNull
	Integer post_id;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public Integer getPost_id() {
		return post_id;
	}
	public void setPost_id(int post_id) {
		this.post_id = post_id;
	}
	
	
}
