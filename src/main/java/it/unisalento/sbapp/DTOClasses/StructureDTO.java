package it.unisalento.sbapp.DTOClasses;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class StructureDTO {

	int id;
	
	@NotNull
	@NotBlank
	String title;
	String description;
	
	Integer user_id;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	
	
	
}
