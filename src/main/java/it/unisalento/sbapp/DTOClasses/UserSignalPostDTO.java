package it.unisalento.sbapp.DTOClasses;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UserSignalPostDTO {

	Integer id;
	@NotNull
	Integer user_id;
	@NotNull
	Integer post_id;
	
	@NotNull
	@NotBlank
	String reason;
	Date dateReporting;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public Integer getPost_id() {
		return post_id;
	}
	public void setPost_id(int post_id) {
		this.post_id = post_id;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Date getDateReporting() {
		return dateReporting;
	}
	public void setDateReporting(Date dateReporting) {
		this.dateReporting = dateReporting;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public void setPost_id(Integer post_id) {
		this.post_id = post_id;
	}
	
	
}
