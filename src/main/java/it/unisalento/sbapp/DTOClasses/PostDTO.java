package it.unisalento.sbapp.DTOClasses;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.PastOrPresent;

public class PostDTO {

	int id;

	@PastOrPresent
	Date pubblicationDate;
	boolean hide;

	String title;

	Integer structure_id;
	Integer user_id;

	String dataJSON;

	List<Integer> idsAttribute;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getPubblicationDate() {
		return pubblicationDate;
	}

	public void setPubblicationDate(Date pubblicationDate) {
		this.pubblicationDate = pubblicationDate;
	}

	public boolean isHide() {
		return hide;
	}

	public void setHide(boolean hide) {
		this.hide = hide;
	}

	public Integer getStructure_id() {
		return structure_id;
	}

	public void setStructure_id(int structure_id) {
		this.structure_id = structure_id;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDataJSON() {
		return dataJSON;
	}

	public void setDataJSON(String dataJSON) {
		this.dataJSON = dataJSON;
	}

	public void setStructure_id(Integer structure_id) {
		this.structure_id = structure_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public List<Integer> getIdsAttribute() {
		return idsAttribute;
	}

	public void setIdsAttribute(List<Integer> idsAttribute) {
		this.idsAttribute = idsAttribute;
	}

}
