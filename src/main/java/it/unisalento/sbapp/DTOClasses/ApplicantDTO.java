package it.unisalento.sbapp.DTOClasses;

import java.util.Date;

public class ApplicantDTO extends UserDTO {

	boolean disable;
	String qualification;
	Date abilitationDate;
	Date blockingStartDate;
	Date blockingEndDate;
	Integer companyId;

	public ApplicantDTO() {
	}

	@Override
	public String toString() {
		return "ApplicantDTO [disable=" + disable + ", qualification=" + qualification + ", abilitationDate="
				+ abilitationDate + ", blockingStartDate=" + blockingStartDate + ", blockingEndDate=" + blockingEndDate
				+ ", companyId=" + companyId + "]";
	}

	public boolean isDisable() {
		return disable;
	}

	public void setDisable(boolean disable) {
		this.disable = disable;
	}

	public Date getBlockingStartDate() {
		return blockingStartDate;
	}

	public void setBlockingStartDate(Date blockingStartDate) {
		this.blockingStartDate = blockingStartDate;
	}

	public Date getBlockingEndDate() {
		return blockingEndDate;
	}

	public void setBlockingEndDate(Date blockingEndDate) {
		this.blockingEndDate = blockingEndDate;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public Date getAbilitationDate() {
		return abilitationDate;
	}

	public void setAbilitationDate(Date abilitationDate) {
		this.abilitationDate = abilitationDate;
	}

}
