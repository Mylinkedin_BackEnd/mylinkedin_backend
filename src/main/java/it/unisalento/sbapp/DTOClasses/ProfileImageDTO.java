package it.unisalento.sbapp.DTOClasses;


public class ProfileImageDTO {


	int id;
	byte[] file;
	Integer user_id;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	
	
}
