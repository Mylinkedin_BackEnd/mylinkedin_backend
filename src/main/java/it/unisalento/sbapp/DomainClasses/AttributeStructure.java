package it.unisalento.sbapp.DomainClasses;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class AttributeStructure {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	
	@ManyToOne(optional = false)
	Attribute attribute;
	
	@ManyToOne(optional = false)
	Structure structure;
	
	public AttributeStructure() {}
	public AttributeStructure(int id, Attribute attribute, Structure structure) {
		super();
		this.id = id;
		this.attribute = attribute;
		this.structure = structure;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Attribute getAttribute() {
		return attribute;
	}

	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	public Structure getStructure() {
		return structure;
	}

	public void setStructure(Structure structure) {
		this.structure = structure;
	}

	
}
