package it.unisalento.sbapp.DomainClasses;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("offeror")
public class Offeror extends User {

	boolean disable;

	Date abilitationDate;
	@ManyToOne(optional = true)
	Company company;
	Date blockingStartDate;
	Date blockingEndDate;

	public Offeror() {
	}

	public Date getAbilitationDate() {
		return abilitationDate;
	}

	public void setAbilitationDate(Date abilitationDate) {
		this.abilitationDate = abilitationDate;
	}

	public void setDisable(boolean disable) {
		this.disable = disable;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Date getBlockingStartDate() {
		return blockingStartDate;
	}

	public void setBlockingStartDate(Date blockingStartDate) {
		this.blockingStartDate = blockingStartDate;
	}

	public Date getBlockingEndDate() {
		return blockingEndDate;
	}

	public void setBlockingEndDate(Date blockingEndDate) {
		this.blockingEndDate = blockingEndDate;
	}

	public boolean isDisable() {
		return disable;
	}
}
