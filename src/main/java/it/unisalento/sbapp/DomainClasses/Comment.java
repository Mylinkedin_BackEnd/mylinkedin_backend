package it.unisalento.sbapp.DomainClasses;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;



@Entity
public class Comment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	
	String text;
	Date insertionDate;
	
	@ManyToOne(optional = true)
	Post post;

	@ManyToOne(optional = true)
	Comment comment;
	
	@OneToMany(mappedBy = "comment",targetEntity = Comment.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	List<Comment> commentList;
	
	@ManyToOne(optional = false)
	User user;
	
	public Comment () {}


	public Comment(int id, String text, Date insertionDate, Post post, Comment comment, List<Comment> commentList,
			User user) {
		super();
		this.id = id;
		this.text = text;
		this.insertionDate = insertionDate;
		this.post = post;
		this.comment = comment;
		this.commentList = commentList;
		this.user = user;
	}




	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getInsertionDate() {
		return insertionDate;
	}

	public void setInsertionDate(Date insertionDate) {
		this.insertionDate = insertionDate;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}
	public Comment getComment() {
		return comment;
	}
	public void setComment(Comment comment) {
		this.comment = comment;
	}
	public List<Comment> getCommentList() {
		return commentList;
	}
	public void setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}




	public User getUser() {
		return user;
	}




	public void setUser(User user) {
		this.user = user;
	}
	
	
	
}
