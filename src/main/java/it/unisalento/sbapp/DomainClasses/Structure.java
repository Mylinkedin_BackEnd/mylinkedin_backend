package it.unisalento.sbapp.DomainClasses;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;


@Entity
public class Structure {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	
	String title;
	String description;
	
	@OneToMany(mappedBy = "structure", targetEntity = Post.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	List<Post> postList;
	
	/*
	@ManyToMany(mappedBy = "structuredList", targetEntity = Attribute.class, fetch = FetchType.LAZY)
	List<Attribute> attributesList;
	*/
	
	@OneToMany(mappedBy = "structure", targetEntity = AttributeStructure.class,  cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<AttributeStructure> attributeStructuresList;
	
	public Structure() {}
	
	public Structure(int id, String title, String description, List<Post> postList,
			List<AttributeStructure> attributeStructuresList) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.postList = postList;
		this.attributeStructuresList = attributeStructuresList;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Post> getPostList() {
		return postList;
	}
	public void setPostList(List<Post> postList) {
		this.postList = postList;
	}
	public List<AttributeStructure> getAttributeStructuresList() {
		return attributeStructuresList;
	}
	public void setAttributeStructuresList(List<AttributeStructure> attributeStructuresList) {
		this.attributeStructuresList = attributeStructuresList;
	}
	
	
	
}
