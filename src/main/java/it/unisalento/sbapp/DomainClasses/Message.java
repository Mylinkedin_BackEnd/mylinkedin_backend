package it.unisalento.sbapp.DomainClasses;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Message {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	
	String text;
	
	Date 	sentDate;
	boolean messageRead;
	
	
	@ManyToOne(optional = false)
	User userSender;
	
	@ManyToOne(optional = true)
	User userReceiver;
	

	public Message() {}



	public boolean isMessage_Read() {
		return messageRead;
	}



	public void setMessage_Read(boolean message_Read) {
		messageRead = message_Read;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public User getUser() {
		return userSender;
	}

	public void setUser(User user) {
		this.userSender = user;
	}

	
	public Date getSentDate() {
		return sentDate;
	}



	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}



	public User getUserSender() {
		return userSender;
	}

	public void setUserSender(User userSender) {
		this.userSender = userSender;
	}

	public User getUserReceiver() {
		return userReceiver;
	}

	public void setUserReceiver(User userReceiver) {
		this.userReceiver = userReceiver;
	}



}
