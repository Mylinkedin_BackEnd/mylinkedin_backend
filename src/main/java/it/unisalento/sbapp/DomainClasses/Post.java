package it.unisalento.sbapp.DomainClasses;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Post {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;

	Date pubblicationDate;
	boolean hide;

	String title;

	String data;

	@ManyToOne(optional = false)
	Structure structure;

	@ManyToOne(optional = false)
	User user;

	@OneToMany(mappedBy = "post", targetEntity = Attached.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<Attached> attachedList;

	@OneToMany(mappedBy = "post", targetEntity = Comment.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<Comment> commentList;

	@OneToMany(mappedBy = "post", targetEntity = UserSelectPost.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<UserSelectPost> UserSelectPostList;

	@OneToMany(mappedBy = "post", targetEntity = UserViewPost.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	List<User> readerList;

	@OneToMany(mappedBy = "post", targetEntity = UserSignalPost.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<UserSelectPost> SignalersList;

	public Post() {
	}

	public Post(int id, Date pubblicationDate, boolean hide, String title, Structure structure, User user,
			List<Attached> attachedList, List<Comment> commentList, List<UserSelectPost> userSelectPostList,
			List<User> readerList, List<UserSelectPost> signalersList) {
		super();
		this.id = id;
		this.pubblicationDate = pubblicationDate;
		this.hide = hide;
		this.title = title;
		this.structure = structure;
		this.user = user;
		this.attachedList = attachedList;
		this.commentList = commentList;
		UserSelectPostList = userSelectPostList;
		this.readerList = readerList;
		SignalersList = signalersList;
	}
	
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getPubblicationDate() {
		return pubblicationDate;
	}

	public void setPubblicationDate(Date pubblicationDate) {
		this.pubblicationDate = pubblicationDate;
	}

	public boolean isHide() {
		return hide;
	}

	public void setHide(boolean hide) {
		this.hide = hide;
	}

	public Structure getStructure() {
		return structure;
	}

	public void setStructure(Structure structure) {
		this.structure = structure;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Attached> getAttachedList() {
		return attachedList;
	}

	public void setAttachedList(List<Attached> attachedList) {
		this.attachedList = attachedList;
	}

	public List<Comment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}

	public List<UserSelectPost> getUserSelectPostList() {
		return UserSelectPostList;
	}

	public void setUserSelectPostList(List<UserSelectPost> userSelectPostList) {
		UserSelectPostList = userSelectPostList;
	}

	public List<User> getReaderList() {
		return readerList;
	}

	public void setReaderList(List<User> readerList) {
		this.readerList = readerList;
	}

	public List<UserSelectPost> getSignalersList() {
		return SignalersList;
	}

	public void setSignalersList(List<UserSelectPost> signalersList) {
		SignalersList = signalersList;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
