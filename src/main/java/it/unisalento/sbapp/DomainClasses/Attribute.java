package it.unisalento.sbapp.DomainClasses;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Attribute {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	
	String name;
	String type;
		
	
	@OneToMany(mappedBy = "attribute", targetEntity = AttributeStructure.class,  cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<AttributeStructure> attributeStructuresList;
	
	
	public Attribute() {}
	
 
	public Attribute(int id, String name, String type  , List<AttributeStructure> attributeStructuresList) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.attributeStructuresList = attributeStructuresList;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	/*public List<Structure> getStructureList() {
		return structureList;
	}
	public void setStructureList(List<Structure> structureList) {
		this.structureList = structureList;
	}*/
	public List<AttributeStructure> getAttributeStructuresList() {
		return attributeStructuresList;
	}
	public void setAttributeStructuresList(List<AttributeStructure> attributeStructuresList) {
		this.attributeStructuresList = attributeStructuresList;
	}

	
	
	
}
