package it.unisalento.sbapp.DomainClasses;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Company {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) // è il db che deve assegnare un id ogni volta che vado a salvare uno user. IDENTITY assegna un id incrementale
	int id; 
	
	@Column(unique = true)
	String name;
	String revenue;
	String webSite;
	String phoneNumber;
	String headquarters;
	String jobSector;
	String legalForm;
	String iva;
	
	
	@OneToMany(mappedBy = "company", targetEntity = Applicant.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<User> applicantEmployees;
	
	@OneToMany(mappedBy = "company", targetEntity = Offeror.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<User> offerorEmployees;
	
	
	
	
	public Company(int id, String name, String webSite, String phoneNumber, String headquarters,
			List<User> applicantEmployees, List<User> offerorEmployees) {
		super();
		this.id = id;
		this.name = name;
		this.webSite = webSite;
		this.phoneNumber = phoneNumber;
		this.headquarters = headquarters;
		this.applicantEmployees = applicantEmployees;
		this.offerorEmployees = offerorEmployees;
	}


	public Company() {}
	

	public String getRevenue() {
		return revenue;
	}


	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}


	public String getJobSector() {
		return jobSector;
	}


	public void setJobSector(String jobSector) {
		this.jobSector = jobSector;
	}


	public String getLegalForm() {
		return legalForm;
	}


	public void setLegalForm(String legalForm) {
		this.legalForm = legalForm;
	}





	public String getIva() {
		return iva;
	}


	public void setIva(String iva) {
		this.iva = iva;
	}


	public List<User> getApplicantEmployees() {
		return applicantEmployees;
	}
	public void setApplicantEmployees(List<User> applicantEmployees) {
		this.applicantEmployees = applicantEmployees;
	}
	public List<User> getOfferorEmployees() {
		return offerorEmployees;
	}
	public void setOfferorEmployees(List<User> offerorEmployees) {
		this.offerorEmployees = offerorEmployees;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getWebSite() {
		return webSite;
	}
	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getHeadquarters() {
		return headquarters;
	}
	public void setHeadquarters(String headquarters) {
		this.headquarters = headquarters;
	}
	
	
	
}
