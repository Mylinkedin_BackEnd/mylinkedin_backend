package it.unisalento.sbapp.DomainClasses;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import it.unisalento.sbapp.MyInterface.IUser;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public class User implements IUser {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	// è il db che deve assegnare un id ogni volta che vado a
	// salvare uno user. IDENTITY assegna un id incrementale
	
	int id;

	String name;
	String surname;
	@Column(unique = true)
	String username;
	String password;
	@Column(unique = true)
	String email;
	String nation;
	Date bornDate;
	Integer CAP;
	String address;
	String city;
	Date registrationDate;

	@Column(unique = true)
	String phoneNumber;

	@Column(insertable = false, updatable = false)
	String type;

	@OneToOne(mappedBy = "user", targetEntity = ProfileImage.class, cascade = CascadeType.ALL)
	ProfileImage profileImage;

	@OneToMany(mappedBy = "user", targetEntity = Post.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<Post> postList;

	@OneToMany(mappedBy = "user", targetEntity = UserViewPost.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<Post> postSeenList;

	@OneToMany(mappedBy = "user", targetEntity = UserSignalPost.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<Post> postSignaledList;

	@OneToMany(mappedBy = "user", targetEntity = Comment.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<Comment> commentList;

	@OneToMany(mappedBy = "user", targetEntity = UserSelectPost.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<UserSelectPost> userSelectPostList;

	@OneToMany(mappedBy = "userSender", targetEntity = Message.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<Message> messageSentList;

	@OneToMany(mappedBy = "userReceiver", targetEntity = Message.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<Message> messageReceivedList;

	// costruttori

	public User() {
	}

	// metodi getter & setter

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBornDate() {
		return bornDate;
	}

	public void setBornDate(Date bornDate) {
		this.bornDate = bornDate;
	}

	public Integer getCAP() {
		return CAP;
	}

	public void setCAP(Integer cAP) {
		CAP = cAP;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public ProfileImage getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(ProfileImage profileImage) {
		this.profileImage = profileImage;
	}

	public List<Post> getPostList() {
		return postList;
	}

	public void setPostList(List<Post> postList) {
		this.postList = postList;
	}

	public List<Comment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}

	public List<UserSelectPost> getUserSelectPostList() {
		return userSelectPostList;
	}

	public void setUserSelectPostList(List<UserSelectPost> userSelectPostList) {
		this.userSelectPostList = userSelectPostList;
	}

	public List<Message> getMessageList() {
		return messageSentList;
	}

	public void setMessageList(List<Message> messageList) {
		this.messageSentList = messageList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nazione) {
		this.nation = nazione;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public List<Message> getMessageSentList() {
		return messageSentList;
	}

	public void setMessageSentList(List<Message> messageSentList) {
		this.messageSentList = messageSentList;
	}

	public List<Message> getMessageReceivedList() {
		return messageReceivedList;
	}

	public void setMessageReceivedList(List<Message> messageReceivedList) {
		this.messageReceivedList = messageReceivedList;
	}

	public List<Post> getPostSeenList() {
		return postSeenList;
	}

	public void setPostSeenList(List<Post> postSeenList) {
		this.postSeenList = postSeenList;
	}

	public List<Post> getPostSignaledList() {
		return postSignaledList;
	}

	public void setPostSignaledList(List<Post> postSignaledList) {
		this.postSignaledList = postSignaledList;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", surname=" + surname + ", username=" + username + ", password="
				+ password + ", email=" + email + ", nation=" + nation + ", bornDate=" + bornDate + ", CAP=" + CAP
				+ ", address=" + address + ", city=" + city + ", registrationDate=" + registrationDate + "]";
	}

}
