package it.unisalento.sbapp.DomainClasses;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class UserSignalPost {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;

	@ManyToOne(optional = false)
	User user;

	@ManyToOne(optional = false)
	Post post;

	@NotNull
	@NotBlank
	String reason;
	Date dateReporting;

	public UserSignalPost() {
	}

	public UserSignalPost(int id, User user, Post post, @NotNull @NotBlank String reason, Date dateReporting) {
		super();
		this.id = id;
		this.user = user;
		this.post = post;
		this.reason = reason;
		this.dateReporting = dateReporting;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getDateReporting() {
		return dateReporting;
	}

	public void setDateReporting(Date dateReporting) {
		this.dateReporting = dateReporting;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

}
