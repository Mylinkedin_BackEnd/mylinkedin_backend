package it.unisalento.sbapp.DomainClasses;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;


@Entity
@DiscriminatorValue("applicant")
public class Applicant extends User {

	boolean disable;
	
	@ManyToOne(optional = true)
	Company company;
	
	Date abilitationDate;
	String qualification;
	Date blockingStartDate;
	Date blockingEndDate;
	
	public Applicant() {}


	public Date getAbilitationDate() {
		return abilitationDate;
	}



	public void setAbilitationDate(Date abilitationDate) {
		this.abilitationDate = abilitationDate;
	}



	public String getQualification() {
		return qualification;
	}



	public void setQualification(String qualification) {
		this.qualification = qualification;
	}



	public boolean isDisable() {
		return disable;
	}

	public void setDisable(boolean disable) {
		this.disable = disable;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
	public Date getBlockingStartDate() {
		return blockingStartDate;
	}

	public void setBlockingStartDate(Date blockingStartDate) {
		this.blockingStartDate = blockingStartDate;
	}

	public Date getBlockingEndDate() {
		return blockingEndDate;
	}

	public void setBlockingEndDate(Date blockingEndDate) {
		this.blockingEndDate = blockingEndDate;
	}

}
