package it.unisalento.sbapp.Iservice;

import java.util.List;

import it.unisalento.sbapp.DomainClasses.Structure;
import it.unisalento.sbapp.Exception.SavingStructureException;
import it.unisalento.sbapp.Exception.StructureNotFoundException;

public interface IStructureService {

	public Structure save(Structure structure) throws SavingStructureException;
	
	public Structure getById(int id) throws StructureNotFoundException;
	
	public List<Structure> getAll();
	
	public boolean delete (int id) throws StructureNotFoundException;
	
	public Structure findByTitle(String title) throws StructureNotFoundException;

}
