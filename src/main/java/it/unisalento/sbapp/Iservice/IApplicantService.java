package it.unisalento.sbapp.Iservice;

import java.util.List;

import it.unisalento.sbapp.DomainClasses.Applicant;
import it.unisalento.sbapp.Exception.SavingUserException;
import it.unisalento.sbapp.Exception.UserNotFoundException;


public interface IApplicantService {

	
	public Applicant save(Applicant applicant) throws SavingUserException;
	
	public Applicant getById(int id) throws UserNotFoundException;
	
	public List<Applicant> getAll();
	
	public boolean delete(int id) throws UserNotFoundException, IllegalArgumentException;
	
	public List<Applicant> getByDisable(boolean disable) throws UserNotFoundException;
	
	public List<Applicant> findByCompanyId(int companyId) throws UserNotFoundException;

	public Applicant findByUsernameAndPassword(String username, String password) throws UserNotFoundException;

}
