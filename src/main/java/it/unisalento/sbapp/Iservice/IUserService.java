package it.unisalento.sbapp.Iservice;

import java.util.List;

import it.unisalento.sbapp.DomainClasses.User;
import it.unisalento.sbapp.Exception.SavingUserException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.MyInterface.IUser;

public interface IUserService {
	
	public User save(User user) throws SavingUserException;
	
	public User getById(int id) throws UserNotFoundException;
	
	public List<User> getAll();
	
	public boolean delete(int id) throws UserNotFoundException,IllegalArgumentException;

	public List<User> getByName(String name) throws UserNotFoundException;
	
	public List<User> getBySurname(String surname) throws UserNotFoundException;
	
	public List<User> getByNameAndSurname(String name, String surname) throws UserNotFoundException;

	public IUser getByUsernameAndPassword(String username, String password) throws UserNotFoundException;
	
	public IUser getByEmailAndPassword(String email, String password) throws UserNotFoundException;
	
	public User getByEmailOrUsernameLike(String email, String username) throws UserNotFoundException;
		
	public User getByUsernameLike(String username) throws UserNotFoundException;

	public boolean modifyPhoneNumber( String phoneNumber, int id) throws UserNotFoundException;
	
	public boolean modifyAddress( String address, int id) throws UserNotFoundException;
	
	public boolean modifyCity( String city, int id) throws UserNotFoundException;


}
