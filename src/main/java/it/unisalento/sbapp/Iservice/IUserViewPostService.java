package it.unisalento.sbapp.Iservice;

import java.util.List;

import it.unisalento.sbapp.DomainClasses.UserViewPost;
import it.unisalento.sbapp.Exception.PostNotFoundException;
import it.unisalento.sbapp.Exception.SavingUserViewPostException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Exception.UserViewPostNotFoundException;

public interface IUserViewPostService {
	
	public UserViewPost save(UserViewPost userViewPost) throws SavingUserViewPostException;
	
	public UserViewPost getById(int id) throws UserViewPostNotFoundException;
	
	public List<UserViewPost> getAll();
	
	public boolean delete(int id) throws UserViewPostNotFoundException;

	public List<UserViewPost> findByPostId(int postId) throws UserViewPostNotFoundException;
	
	public List<UserViewPost> findByUserId(int userId) throws UserViewPostNotFoundException;

}
