package it.unisalento.sbapp.Iservice;

import java.util.List;

import it.unisalento.sbapp.DomainClasses.UserSelectPost;
import it.unisalento.sbapp.Exception.PostNotFoundException;
import it.unisalento.sbapp.Exception.SavingUserSelectPostException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Exception.UserSelectPostNotFoundException;


public interface IuserSelectPostService {
	
	public UserSelectPost save(UserSelectPost userSelectPost) throws SavingUserSelectPostException;
	
	public UserSelectPost getById(int id) throws UserSelectPostNotFoundException;
	
	public List<UserSelectPost> getAll();
	
	public boolean delete(int id) throws UserSelectPostNotFoundException;

	public List<UserSelectPost> findByUserId(int userId) throws UserNotFoundException;
	
	public List<UserSelectPost> findByUserUsername(String userUsername) throws UserNotFoundException;
	
	public List<UserSelectPost> findByPostId(int postId) throws PostNotFoundException;
}
