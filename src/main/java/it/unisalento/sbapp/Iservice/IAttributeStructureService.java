package it.unisalento.sbapp.Iservice;

import java.util.List;

import it.unisalento.sbapp.DomainClasses.Attribute;
import it.unisalento.sbapp.DomainClasses.AttributeStructure;
import it.unisalento.sbapp.DomainClasses.Structure;
import it.unisalento.sbapp.Exception.AttributeStructureNotFound;
import it.unisalento.sbapp.Exception.SavingAttributeStructureException;



public interface IAttributeStructureService {

	public AttributeStructure save(AttributeStructure attributeStructure) throws SavingAttributeStructureException;
	
	public AttributeStructure getById(int id) throws AttributeStructureNotFound;
		
	public List<AttributeStructure> getAll();

	public boolean delete (int id) throws AttributeStructureNotFound,IllegalArgumentException;
	
	public List<AttributeStructure> findByStructureId(int structureId) throws AttributeStructureNotFound;

	public List<AttributeStructure> findByStructureTitle(String structureTitle) throws AttributeStructureNotFound;
	
	public void createBound(Structure structure, Attribute relatedAttribute) throws IllegalArgumentException;

}
