package it.unisalento.sbapp.Iservice;

import java.util.List;

import org.springframework.data.repository.query.Param;

import it.unisalento.sbapp.DomainClasses.UserSignalPost;
import it.unisalento.sbapp.Exception.SavingUserSignalPostException;
import it.unisalento.sbapp.Exception.UserSignalPostNotFoundException;



public interface IUserSignalPostService {
	
	public UserSignalPost save(UserSignalPost userSignalPost) throws SavingUserSignalPostException;
	
	public UserSignalPost getById(int id) throws UserSignalPostNotFoundException;
	
	public List<UserSignalPost> findUserSignaledPostByIdPost(int id_Post)throws UserSignalPostNotFoundException;
	
	public List<UserSignalPost> findPostSignaledByUser(int id_User)throws UserSignalPostNotFoundException;

	public List<UserSignalPost> getAll() ;
	
	public boolean delete(int id) throws UserSignalPostNotFoundException;


}
