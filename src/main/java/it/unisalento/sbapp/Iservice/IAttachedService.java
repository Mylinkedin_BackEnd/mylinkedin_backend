package it.unisalento.sbapp.Iservice;

import java.util.List;

import it.unisalento.sbapp.DomainClasses.Attached;
import it.unisalento.sbapp.DomainClasses.Post;
import it.unisalento.sbapp.Exception.AttachedNotFoundException;
import it.unisalento.sbapp.Exception.PostNotFoundException;
import it.unisalento.sbapp.Exception.SavingAttachedException;

public interface IAttachedService {
	
	public Attached save(Attached attached) throws SavingAttachedException;
	
	public Attached getById(int id) throws AttachedNotFoundException;
		
	public List<Attached> getAll();

	public boolean delete (int id) throws AttachedNotFoundException,IllegalArgumentException;
	
	public List<Attached> getByPost(Post post) throws PostNotFoundException;

	public List<Attached> findByPostId(int postId) throws PostNotFoundException;

}
