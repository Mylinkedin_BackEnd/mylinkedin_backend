package it.unisalento.sbapp.Iservice;

import java.util.List;

import it.unisalento.sbapp.DomainClasses.Message;
import it.unisalento.sbapp.DomainClasses.User;
import it.unisalento.sbapp.Exception.MessageNotFoundException;
import it.unisalento.sbapp.Exception.SavingMessageException;
import it.unisalento.sbapp.Exception.UserNotFoundException;

public interface IMessageService {
	
	
	public Message save(Message message) throws SavingMessageException;
	
	public Message getById(int id) throws MessageNotFoundException;
		
	public List<Message> getAll();

	public boolean delete (int id) throws MessageNotFoundException,IllegalArgumentException;
	
	public List<Message> findByUserSender(User userSender) throws UserNotFoundException;
	
	public List<Message> findByUserSenderId(int userSender_id) throws UserNotFoundException;

	public List<Message> findByUserReceiver(User userReceiver) throws UserNotFoundException;

	public List<Message> findByUserReceiverId(int userReceiver_id) throws UserNotFoundException;
	
	public List<Message> findByUserReceiverIdAndMessageRead(int userReceiver_id, boolean messageRead) throws UserNotFoundException;

	public List<Message> findByUserReceiverIdAndUserSenderId(int userReceiver_id, int userSender_id) throws UserNotFoundException;

	public List<Message> findByUserReceiverUsernameLikeAndUserSenderUsernameLike (String userReceiverUsername, String userSenderUsername) throws UserNotFoundException;

	public List<Message> findByUserReceiverUsernameAndUserSenderId(String receiverUsername, int senderId) throws UserNotFoundException;

}
