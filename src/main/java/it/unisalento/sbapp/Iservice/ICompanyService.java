package it.unisalento.sbapp.Iservice;

import java.util.List;

import it.unisalento.sbapp.DomainClasses.Company;
import it.unisalento.sbapp.Exception.CompanyNotFoundException;
import it.unisalento.sbapp.Exception.SavingCompanyException;


public interface ICompanyService {

	public Company save(Company company) throws SavingCompanyException;
	
	public Company getById(int id) throws CompanyNotFoundException;
		
	public List<Company> getAll();

	public boolean delete (int id) throws CompanyNotFoundException, IllegalArgumentException;
	
	public Company getByName(String companyName) throws CompanyNotFoundException;
	
	public Company findByIva(String iva) throws CompanyNotFoundException;

	
}
