package it.unisalento.sbapp.Iservice;

import java.util.List;

import it.unisalento.sbapp.DomainClasses.Offeror;
import it.unisalento.sbapp.Exception.SavingUserException;
import it.unisalento.sbapp.Exception.UserNotFoundException;


public interface IOfferorService {

	public Offeror save(Offeror offeror) throws SavingUserException;
	
	public Offeror getById(int id) throws UserNotFoundException;
	
	public List<Offeror> getAll();
	
	public boolean delete(int id) throws UserNotFoundException;
	
	public List<Offeror> findByDisable(boolean disable)throws UserNotFoundException;

	public List<Offeror> findByCompanyId(int companyId)throws UserNotFoundException;
	
	public Offeror findByUsernameAndPassword(String username, String password) throws UserNotFoundException;


}
