package it.unisalento.sbapp.Iservice;

import java.util.List;

import it.unisalento.sbapp.DomainClasses.Attribute;
import it.unisalento.sbapp.Exception.AttributeNotFoundException;
import it.unisalento.sbapp.Exception.SavingAttributeException;

public interface IAttributeService {
	
	public Attribute save(Attribute attribute) throws SavingAttributeException;
	
	public Attribute getById(int id) throws AttributeNotFoundException;
		
	public List<Attribute> getAll();

	public boolean delete (int id) throws AttributeNotFoundException,IllegalArgumentException;
	
	public Attribute getByNameLike(String name) throws AttributeNotFoundException;


}
