
package it.unisalento.sbapp.Iservice;

import java.util.List;

import it.unisalento.sbapp.DomainClasses.Administrator;
import it.unisalento.sbapp.Exception.SavingUserException;
import it.unisalento.sbapp.Exception.UserNotFoundException;

public interface IAdministratorService {

	public Administrator save(Administrator comment) throws SavingUserException;

	public Administrator getById(int id) throws UserNotFoundException;

	public List<Administrator> getAll();

	public boolean delete(int id) throws UserNotFoundException, IllegalArgumentException;

	public Administrator findByUsernameAndPassword(String username, String password) throws UserNotFoundException;

	public boolean EnableUserByUsername(String username) throws UserNotFoundException;

	public boolean EnableUserById(int id_User) throws UserNotFoundException;

	public boolean lockUser(String username ) throws UserNotFoundException;

	public boolean lockUserById(int id ) throws UserNotFoundException;


	public boolean unlockUser(String username) throws UserNotFoundException;

	public boolean unlockUserById(int id) throws UserNotFoundException;
	
}