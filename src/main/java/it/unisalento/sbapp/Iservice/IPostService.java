package it.unisalento.sbapp.Iservice;

import java.util.Date;
import java.util.List;

import it.unisalento.sbapp.DomainClasses.Post;
import it.unisalento.sbapp.DomainClasses.User;
import it.unisalento.sbapp.Exception.PostNotFoundException;
import it.unisalento.sbapp.Exception.SavingPostException;
import it.unisalento.sbapp.Exception.StructureNotFoundException;
import it.unisalento.sbapp.Exception.UserNotFoundException;

public interface IPostService {
	
	public Post save(Post post) throws SavingPostException;
	
	public Post getById(int id) throws PostNotFoundException;
	
	public List<Post> getAll();
	
	public boolean deleteById (int id) throws PostNotFoundException;
	
	public List<Post> findByUserName(String userName) throws UserNotFoundException;
	
	public List<Post> findByUserId(int userId) throws UserNotFoundException;

	public boolean delete(Post post) throws PostNotFoundException;
	
	public List<Post> findByUser(User user) throws UserNotFoundException;
	
	public List<Post> findByPubblicationDateAfter(Date pubblicationDate) throws PostNotFoundException;

	public List<Post> findByPubblicationDateAfterAndStructureId(Date pubblicationDate, int structureId) throws StructureNotFoundException;

	public List<Post> findByPubblicationDateAfterAndUserId(Date pubblicationDate, int userId) throws UserNotFoundException;
	
	public List<Post> findByPubblicationDateAfterAndStructureIdAndUserId(Date pubblicationDate, int structureId, int userId) throws UserNotFoundException,StructureNotFoundException;

	public List<Post> findByStructureIdAndUserId(int structureId, int userId) throws UserNotFoundException,StructureNotFoundException;
	
	public void ModifyPostWithQuery(String title, int id) throws PostNotFoundException;


}
