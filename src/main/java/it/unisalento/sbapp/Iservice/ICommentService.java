package it.unisalento.sbapp.Iservice;

import java.util.List;

import it.unisalento.sbapp.DomainClasses.Comment;
import it.unisalento.sbapp.DomainClasses.Post;
import it.unisalento.sbapp.Exception.CommentNotFoundException;
import it.unisalento.sbapp.Exception.PostNotFoundException;
import it.unisalento.sbapp.Exception.SavingCommentException;

public interface ICommentService {
	
	public Comment save(Comment comment) throws SavingCommentException;
	
	public Comment getById(int id) throws CommentNotFoundException;
		
	public List<Comment> getAll();

	public boolean delete (int id) throws CommentNotFoundException, IllegalArgumentException;
	
	public List<Comment> getByPostId(int postId) throws PostNotFoundException;
	
	public List<Comment> getByPost(Post post) throws PostNotFoundException;
	
	public List<Comment> getByCommentId(int commentId) throws CommentNotFoundException;
	
	public List<Comment> getByComment(Comment comment) throws CommentNotFoundException;



}
