package it.unisalento.sbapp.Iservice;

import java.util.List;

import it.unisalento.sbapp.DomainClasses.ProfileImage;
import it.unisalento.sbapp.DomainClasses.User;
import it.unisalento.sbapp.Exception.ProfileImageNotFoundException;
import it.unisalento.sbapp.Exception.SavingProfileImageException;
import it.unisalento.sbapp.Exception.UserNotFoundException;

public interface IProfileImageService {
	
	public ProfileImage save(ProfileImage profileImage) throws SavingProfileImageException;
	
	public ProfileImage getById(int id) throws ProfileImageNotFoundException;
		
	public List<ProfileImage> getAll();

	public boolean delete (int id) throws ProfileImageNotFoundException;

	public ProfileImage findByUser(User user) throws UserNotFoundException;
	
	public ProfileImage findByUserId(int userId) throws UserNotFoundException;
	
	public ProfileImage findByUserUsername(String userUsername)throws UserNotFoundException;

}
