package it.unisalento.sbapp.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.unisalento.sbapp.DomainClasses.UserViewPost;

public interface UserViewPostRepository extends JpaRepository<UserViewPost, Integer>{

	public List<UserViewPost> findByPostId(int postId);
	
	public List<UserViewPost> findByUserId(int userId);
}
 