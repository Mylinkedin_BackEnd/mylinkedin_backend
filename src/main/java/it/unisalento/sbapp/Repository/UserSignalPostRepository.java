package it.unisalento.sbapp.Repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.unisalento.sbapp.DomainClasses.UserSignalPost;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Exception.UserSignalPostNotFoundException;

public interface UserSignalPostRepository extends JpaRepository<UserSignalPost, Integer>{
	
	
	//ritorna la lista di UserSignalPost che hanno segnlato l'utente
	@Query("select u from UserSignalPost u where u.post.id= :id_Post")
	public List<UserSignalPost> findUserSignaledPostByIdPost(@Param("id_Post")int id_Post)throws UserSignalPostNotFoundException;
	
	//ritorna la lista di post segnalati da un utente
	@Query("select u from UserSignalPost u where u.user.id= :id_user")
	public List<UserSignalPost> findPostSignaledByUser(@Param("id_user")int id_User)throws UserSignalPostNotFoundException;

}
