package it.unisalento.sbapp.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.unisalento.sbapp.DomainClasses.Administrator;
import it.unisalento.sbapp.DomainClasses.Applicant;

public interface ApplicantRepository extends JpaRepository<Applicant, Integer>{

	public List<Applicant> findByDisable(boolean disable);
	
	public List<Applicant> findByCompanyId(int companyId);
	
	public Applicant findByUsernameAndPassword(String username, String password);

}
