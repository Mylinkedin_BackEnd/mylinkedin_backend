package it.unisalento.sbapp.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.unisalento.sbapp.DomainClasses.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	
	// METODO 1: JPQL
//	@Query("select u from IUserDTO u where u.name like :name")//from "classe di domain"
//	public List<User> findByNameWithQuery(@Param("name") String name);
	
	// METODO 2 
	public List<User> findByNameLike(String name);  // è equivalente, parola magica: findBy
	
	public List<User> findBySurnameLike(String surname);
	
	public List<User> findByNameAndSurnameLike(String name, String Surname);
	
	public User findByUsernameAndPasswordLike(String username, String password);
	
	public User findByEmailAndPasswordLike( String email, String password);
			
	public User findByEmailOrUsernameLike(String email, String username);
	
	public User findByUsernameLike(String username); 
	
	@Query("UPDATE User u SET u.phoneNumber = :phoneNumber   WHERE u.id = :id ")
	@Modifying
	public int ModifyPhoneNumberWithQuery(@Param("phoneNumber") String phoneNumber, @Param("id") int id);

	@Query("UPDATE User u SET u.address = :address   WHERE u.id = :id ")
	@Modifying
	public int ModifyAddressWithQuery(@Param("address") String address, @Param("id") int id);
	
	@Query("UPDATE User u SET u.city = :city   WHERE u.id = :id ")
	@Modifying
	public int ModifyCityWithQuery(@Param("city") String city, @Param("id") int id);

	


}
