package it.unisalento.sbapp.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.unisalento.sbapp.DomainClasses.UserSelectPost;

public interface UserSelectPostRepository extends JpaRepository<UserSelectPost, Integer>{

	public List<UserSelectPost> findByUserId(int userId);
	
	public List<UserSelectPost> findByUserUsername(String userUsername);
	
	public List<UserSelectPost> findByPostId(int postId);
	
}
