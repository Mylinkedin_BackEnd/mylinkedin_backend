package it.unisalento.sbapp.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.unisalento.sbapp.DomainClasses.Comment;
import it.unisalento.sbapp.DomainClasses.Post;
import it.unisalento.sbapp.Exception.CommentNotFoundException;

public interface CommentRepository extends JpaRepository<Comment, Integer>{
	
	public List<Comment> findByPostId(int postId);
	
	public List<Comment> findByPost(Post post);
	
//	trova tutti i commenti legati ad un commento
	public List<Comment> findByCommentId(int commentId);
	
	public List<Comment> findByComment(Comment comment);

	@Query("select c from Comment c where c.user.username= :username")
	public List<Comment> findCommentOfUser( @Param("username")String username)throws CommentNotFoundException;

}
