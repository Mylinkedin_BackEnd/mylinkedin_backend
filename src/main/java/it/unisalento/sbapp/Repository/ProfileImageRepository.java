package it.unisalento.sbapp.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import it.unisalento.sbapp.DomainClasses.ProfileImage;
import it.unisalento.sbapp.DomainClasses.User;

public interface ProfileImageRepository extends JpaRepository<ProfileImage, Integer> {
	
	public ProfileImage findByUser(User user);
	
	public ProfileImage findByUserId(int userId);
	
	public ProfileImage findByUserUsernameLike(String userUsername);

}
