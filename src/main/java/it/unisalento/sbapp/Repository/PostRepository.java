package it.unisalento.sbapp.Repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.unisalento.sbapp.DTOClasses.StructureDTO;
import it.unisalento.sbapp.DomainClasses.Post;
import it.unisalento.sbapp.DomainClasses.User;
import it.unisalento.sbapp.Exception.StructureNotFoundException;

public interface PostRepository extends JpaRepository<Post, Integer> {
		
	public List<Post> findByUserId(int userId);
		
	public List<Post> findByUserName(String userName);
	
	public List<Post> findByUser(User user);
	
//	mostra post dopo una determinata data
	public List<Post> findByPubblicationDateAfter(Date pubblicationDate);
	
	public List<Post> findByPubblicationDateAfterAndStructureId(Date pubblicationDate, int structureId);

	public List<Post> findByPubblicationDateAfterAndUserId(Date pubblicationDate, int userId);
	
	public List<Post> findByPubblicationDateAfterAndStructureIdAndUserId(Date pubblicationDate, int structureId, int userId);

	public List<Post> findByStructureIdAndUserId(int structureId, int userId); 
	
	@Query("select pt.structure from Post pt where pt.structure.id=:id")
	public StructureDTO getStructureFromPost(@Param("id") int id)throws StructureNotFoundException;
	
//******************	MODIFICARE IL TESTO DI UN POST	***********************************
	
	@Query("UPDATE Post p SET p.title = :title   WHERE p.id = :id ")
	@Modifying
	public void ModifyPostWithQuery(@Param("title") String title, @Param("id") int id);
	 
}
