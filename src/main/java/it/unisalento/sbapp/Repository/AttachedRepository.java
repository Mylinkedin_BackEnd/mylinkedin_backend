package it.unisalento.sbapp.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.unisalento.sbapp.DomainClasses.Attached;
import it.unisalento.sbapp.DomainClasses.Post;

@Repository
public interface AttachedRepository extends JpaRepository<Attached, Integer>{
	
	public List<Attached> findByPost(Post post);
	
	public List<Attached> findByPostId(int postId);
	

}
