package it.unisalento.sbapp.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.unisalento.sbapp.DomainClasses.Offeror;

public interface OfferorRepository extends JpaRepository<Offeror, Integer>{

	public List<Offeror> findByDisable(boolean disable);

	public List<Offeror> findByCompanyId(int companyId);
	
	public Offeror findByUsernameAndPassword(String username, String password);
}
