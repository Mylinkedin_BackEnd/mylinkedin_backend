package it.unisalento.sbapp.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.unisalento.sbapp.DomainClasses.AttributeStructure;

@Repository
public interface AttributeStructureRepository extends JpaRepository<AttributeStructure, Integer>{

//	LISTA DI ATTRIBUTI COLLEGATI ALLA STRUTTURA RICHIESTA
	public List<AttributeStructure> findByStructureId(int structureId);

	public List<AttributeStructure> findByStructureTitle(String structureTitle);
}
