package it.unisalento.sbapp.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.unisalento.sbapp.DomainClasses.Message;
import it.unisalento.sbapp.DomainClasses.User;

public interface MessageRepository extends JpaRepository<Message, Integer>{
	
	public List<Message> findByUserSender(User userSender);
	
	public List<Message> findByUserSenderId(int userSender_id);

	public List<Message> findByUserReceiver(User userReceiver);

//	trova tutti i messaggi ricevuti da uno user
	public List<Message> findByUserReceiverId(int userReceiver_id);
	
	public List<Message> findByUserReceiverIdAndMessageRead(int userReceiver_id, boolean messageRead);
	
//	utile nelle chat
	public List<Message> findByUserReceiverIdAndUserSenderId(int userReceiver_id, int userSender_id);

	public List<Message> findByUserReceiverUsernameLikeAndUserSenderUsernameLike (String userReceiverUsername, String userSenderUsername);

	public List<Message> findByUserReceiverUsernameAndUserSenderId(String receiverUsername, int senderId);
}
