package it.unisalento.sbapp.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.unisalento.sbapp.DomainClasses.Attribute;

@Repository
public interface AttributeRepository extends JpaRepository<Attribute, Integer>{
	
	public Attribute findByNameLike(String name);
	
}
