package it.unisalento.sbapp.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import it.unisalento.sbapp.DomainClasses.Structure;

public interface StructureRepository extends JpaRepository<Structure, Integer>{

	public Structure findByTitle(String title);
}
