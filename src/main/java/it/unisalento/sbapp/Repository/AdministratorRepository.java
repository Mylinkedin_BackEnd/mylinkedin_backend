package it.unisalento.sbapp.Repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.unisalento.sbapp.DomainClasses.Administrator;
import it.unisalento.sbapp.Exception.UserNotFoundException;

public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {

	public Administrator findByUsernameAndPassword(String username, String password);

	/*********************** enable user method *************************/

	@Query("UPDATE User u SET u.disable = false , u.abilitationDate= :date  WHERE u.username = :username ")
	@Modifying
	public int EnableUser(@Param("username") String username, @Param("date") Date abilitationDate)
			throws UserNotFoundException;

	@Query("UPDATE User u SET u.disable = false ,u.abilitationDate= :date  WHERE u.id = :id ")
	@Modifying
	public int EnableUserById(@Param("id") int id_user, @Param("date") Date abilitationDate)
			throws UserNotFoundException;

	/************************* lock user method ****************/

	@Query("UPDATE User u SET u.disable = true ,u.blockingStartDate= :date  WHERE u.username = :username ")
	@Modifying
	public int lockUser(@Param("username") String username, @Param("date") Date startBlockingDate)
			throws UserNotFoundException;

	@Query("UPDATE User u SET u.disable = true ,u.blockingStartDate= :date  WHERE u.id = :id ")
	@Modifying
	public int lockUserById(@Param("id") int id_user, @Param("date") Date startBlockingDate)
			throws UserNotFoundException;

	/****************************** unlock method user *******************/

	@Query("UPDATE User u SET u.disable = false ,u.blockingEndDate= :date  WHERE u.id = :id ")
	@Modifying
	public int unlockUserById(@Param("id") int id_user, @Param("date") Date startBlockingDate)
			throws UserNotFoundException;
	
	@Query("UPDATE User u SET u.disable = false ,u.blockingEndDate= :date  WHERE u.username = :username ")
	@Modifying
	public int unlockUser(@Param("username") String username, @Param("date") Date startBlockingDate)
			throws UserNotFoundException;

}
