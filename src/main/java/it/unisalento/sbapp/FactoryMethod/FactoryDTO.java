package it.unisalento.sbapp.FactoryMethod;

import it.unisalento.sbapp.DTOClasses.*;
import it.unisalento.sbapp.MyInterface.*;

public class FactoryDTO {

	public UserDTO getDTO(String type) {
	
		 if (type == null) {
	            return null;
	        }
	        if (type.equalsIgnoreCase("admin")) {
	            return new AdministratorDTO();
	        } else if (type.equalsIgnoreCase("offeror")) {
	            return new OfferorDTO();
	        } else if (type.equalsIgnoreCase("applicant")) {
	            return new ApplicantDTO();
	        }
	        return null;
	}

}
