package it.unisalento.sbapp.FactoryMethod;

import it.unisalento.sbapp.DomainClasses.*;
import it.unisalento.sbapp.MyInterface.IUser;

public class Factory {

	public User getUser(String IUserType) {
		 if (IUserType == null) {
	            return null;
	        }
	        if (IUserType.equalsIgnoreCase("admin")) {
	            return new Administrator();
	        } else if (IUserType.equalsIgnoreCase("applicant")) {
	            return new Applicant();
	        } else if (IUserType.equalsIgnoreCase("offeror")) {
	            return new Offeror();
	        }
	        return null;
	}
	
}
