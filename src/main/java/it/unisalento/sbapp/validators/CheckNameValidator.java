package it.unisalento.sbapp.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CheckNameValidator implements ConstraintValidator<CheckName, String>{

	
	String [] nameToCheck;
	
		@Override
	public void initialize(CheckName constraintAnnotation) {
		// TODO Auto-generated method stub
		this.nameToCheck = constraintAnnotation.nameToCheck();
	}
	
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		
		for (String string : nameToCheck) {
			
		if(value.toLowerCase().equals(string.toLowerCase())) {
			return false;
			}
		}
		return true;	
	}
}
