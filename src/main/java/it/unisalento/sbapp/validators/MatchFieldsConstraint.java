package it.unisalento.sbapp.validators;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Target(ElementType.TYPE) // questa volta VALIDATION su una CLASSE
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MatchFieldsValidator.class)
public @interface MatchFieldsConstraint {

	String field ();
	String fieldMatch();
	
	String message() default "i campi devono essere uguali";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
