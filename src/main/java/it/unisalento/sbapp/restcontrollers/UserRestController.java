package it.unisalento.sbapp.RestControllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import it.unisalento.sbapp.DTOClasses.UserDTO;
import it.unisalento.sbapp.DomainClasses.Administrator;
import it.unisalento.sbapp.DomainClasses.Applicant;
import it.unisalento.sbapp.DomainClasses.Offeror;
import it.unisalento.sbapp.DomainClasses.User;
import it.unisalento.sbapp.DomainClasses.UserSelectPost;
import it.unisalento.sbapp.DomainClasses.UserViewPost;
import it.unisalento.sbapp.Exception.PostNotFoundException;
import it.unisalento.sbapp.Exception.SavingUserException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Exception.UserViewPostNotFoundException;
import it.unisalento.sbapp.FactoryMethod.Factory;
import it.unisalento.sbapp.FactoryMethod.FactoryDTO;
import it.unisalento.sbapp.Iservice.*;
import it.unisalento.sbapp.MyInterface.IUser;
import it.unisalento.sbapp.MyInterface.IUserDTO;


@RestController
@RequestMapping("/user")
public class UserRestController {
	
	@Autowired
	IUserService userService;
	
	@Autowired
	IuserSelectPostService userSelectPostService;
	
	@Autowired
	IUserViewPostService userViewPostService;
	
	Factory userDomainFactory;
	FactoryDTO userDTOFactory;
	private ModelMapper modelMapper = new ModelMapper();
	
	@RequestMapping(value="/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public UserDTO get(@PathVariable int id  ) throws UserNotFoundException{
		
		
		
		User user = userService.getById(id);
		
		// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO
		
		UserDTO userDTO = modelMapper.map(user, UserDTO.class);
		userDTO.setPassword("*******");

		
		return userDTO;
	}
	
//	CERCO UNO USER DA NOME E COGNOME
	@RequestMapping(value ="/getByNameAndSurname/{name}/{surname}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserDTO> getByNameAndSurname(@PathVariable String surname, @PathVariable String name) throws UserNotFoundException {


		List<UserDTO> listDTO = new ArrayList<UserDTO>();
		List<User> list = new ArrayList<User>();

		list = userService.getByNameAndSurname(name, surname);
		
		for (User user : list) {
			
			UserDTO userDTO = modelMapper.map(user, UserDTO.class);
			listDTO.add(userDTO);
		}
		
		return listDTO;
		
		
	}
	
//	CERCO UNO USER PER USERNAME E PASSWORD

	// metodo di LOGIN

	@PostMapping(value = "/getByUsernameAndPassword", produces = MediaType.APPLICATION_JSON_VALUE)
	public IUserDTO getByUsernameAndPassword(@RequestBody UserDTO userToLogin) throws UserNotFoundException {

		try {
			userDTOFactory = new FactoryDTO();

			IUser user = userService.getByUsernameAndPassword(userToLogin.getUsername(), userToLogin.getPassword());

			IUserDTO userDTO = userDTOFactory.getDTO(user.getType());
			modelMapper.map(user, userDTO);

			return userDTO;

		} catch (Exception e) {
			e.printStackTrace();
			throw new UserNotFoundException();
		}

	}
	
//	CERCO UNO USER PER EMAIL E PASSWORD
	@RequestMapping(value ="/getByEmailAndPassword/{email}/{password}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public IUserDTO getByEmailAndPassword(@RequestBody UserDTO userDTO ) throws UserNotFoundException {

		try {
			userDTOFactory = new FactoryDTO();

			IUser user = userService.getByEmailAndPassword(userDTO.getEmail(), userDTO.getPassword());
			IUserDTO DTOUser = userDTOFactory.getDTO(user.getType());
			modelMapper.map(user, DTOUser);
			return DTOUser;
			
		} catch (Exception e) {
			throw new UserNotFoundException();
		}
		
	}
	
//	CERCO UNO USER PER USERNAME || EMAIL
	@RequestMapping(value ="/getByEmailOrUsername/{username}/{email}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public UserDTO getByEmailOrUsername(@PathVariable String email, @PathVariable String username ) throws UserNotFoundException {

		try {
			User user = userService.getByEmailOrUsernameLike(email, username);
			UserDTO userDTO = modelMapper.map(user, UserDTO.class);
			return userDTO;
			
		} catch (Exception e) {
			throw new UserNotFoundException();
		}
		
	}

//	CERCO UNO USER PER NOME
	@RequestMapping(value ="/getByName/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserDTO> getByName(@PathVariable String name) throws UserNotFoundException {


		List<UserDTO> listDTO = new ArrayList<UserDTO>();
		List<User> list = new ArrayList<User>();

		list = userService.getByName(name);
		
		for (User user : list) {
			
			UserDTO userDTO = modelMapper.map(user, UserDTO.class);
			listDTO.add(userDTO);
		}
		
		return listDTO;
		
		
	}
	
//	CERCO UNO USER PER USERNAME
	@RequestMapping(value ="/getByUsername/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public UserDTO getByUsername(@PathVariable String username) throws UserNotFoundException {


		try {
			User user = userService.getByUsernameLike(username);
			UserDTO userDTO = modelMapper.map(user, UserDTO.class);
			return userDTO;
			
		} catch (Exception e) {
			throw new UserNotFoundException();
		}
		
	}
	
//	CERCO UNO USER PER COGNOME
	@RequestMapping(value ="/getBySurname/{surname}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserDTO> getBySurname(@PathVariable String surname) throws UserNotFoundException {


		List<UserDTO> listDTO = new ArrayList<UserDTO>();
		List<User> list = new ArrayList<User>();

		list = userService.getBySurname(surname);
		
		for (User user : list) {
			
			UserDTO userDTO = modelMapper.map(user, UserDTO.class);
			listDTO.add(userDTO);
		}
		
		return listDTO;
		
		
	}
	
//	CERCO TUTTI GLI USER CHE HANNO SELEZIONATO UN POST DAL {/postId}
	@RequestMapping(value ="/getUsersWhoSelectPostById/{postId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserDTO> getUsersWhoSelectPostById(@PathVariable int postId) throws PostNotFoundException, UserNotFoundException {

		List<UserSelectPost> 	userSelectPostsList = userSelectPostService.findByPostId(postId);
		
		List<UserDTO> userDTOList = new ArrayList<UserDTO>();
		
		
		for (UserSelectPost userSelectPost : userSelectPostsList) {
			
			User user = userSelectPost.getUser();
			UserDTO userDTO = modelMapper.map(user, UserDTO.class);
			
			userDTOList.add(userDTO);
		}
		
		return userDTOList;
	}
	
	//Dato un post, cerco gli utenti che lo hanno visualizzato
		@RequestMapping(value ="/getUsersWhoViewPostById/{postId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public List<UserDTO> getUsersWhoViewPostById(@PathVariable int postId) throws  UserViewPostNotFoundException {

			List<UserViewPost> 	userViewPostsList = userViewPostService.findByPostId(postId);
			
			List<UserDTO> userDTOList = new ArrayList<UserDTO>();
			
			
			for (UserViewPost userViewPost : userViewPostsList) {
				
				User user = userViewPost.getUser();
				UserDTO userDTO = modelMapper.map(user, UserDTO.class);
				
				userDTOList.add(userDTO);
			}
			
			return userDTOList;
		}
	

//  CERCO TUTTI GLI USER
	@RequestMapping(value="/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserDTO> getAll( ){
		
		List<User> 	userList = userService.getAll();
		List<UserDTO> userDTOList = new ArrayList<UserDTO>(); 
		
		for (User user : userList) {
			
			UserDTO userDTO = modelMapper.map(user, UserDTO.class);
			userDTOList.add(userDTO);
		}
			
		
		return userDTOList;
	}

	
	
	// come passare parametri nel body della request
	// quando possiamo farlo ? solo in 1 caso : quando il method è un POST
	
//	SALVARE UNO USER
	@PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public boolean save(@RequestBody @Valid UserDTO userDTO) throws SavingUserException {

		if (userDTO.getType().equalsIgnoreCase("admin")) {
			Administrator admin = modelMapper.map(userDTO, Administrator.class);
			admin.setRegistrationDate(new Date());
			userService.save(admin);
			return true;
		}
		if (userDTO.getType().equalsIgnoreCase("applicant")) {
			Applicant appli = modelMapper.map(userDTO, Applicant.class);
			appli.setRegistrationDate(new Date());
			appli.setDisable(true);
			userService.save(appli);
			return true;
		}
		Offeror off = modelMapper.map(userDTO, Offeror.class);
		off.setRegistrationDate(new Date());
		off.setDisable(true);

		userService.save(off);
		return true;

	}
	
//	RIMUOVERE UN UTENTE
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	public  ResponseEntity<User> delete(@PathVariable int id ) throws UserNotFoundException, IllegalArgumentException{
		
		userService.delete(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
//	**********     MODIFICARE IL NUMERO DI TELEFONO DI UN UTENTE 		*********
	
	@PostMapping(value = "/modifyPhoneNumber",  consumes = MediaType.APPLICATION_JSON_VALUE)
	public UserDTO modifyPhoneNumber(@RequestBody UserDTO userDTO) throws UserNotFoundException {
		
		userService.modifyPhoneNumber(userDTO.getPhoneNumber(), userDTO.getId());
		User user = userService.getById(userDTO.getId());
		
		userDTO = modelMapper.map(user, UserDTO.class);
		return userDTO;
	}

//	**********     MODIFICARE LA CITTA' DI UN UTENTE 		*********
	
	@PostMapping(value = "/modifyCity",  consumes = MediaType.APPLICATION_JSON_VALUE)
	public UserDTO modifyCity(@RequestBody UserDTO userDTO) throws UserNotFoundException {
		
		userService.modifyCity(userDTO.getCity(), userDTO.getId());
		User user = userService.getById(userDTO.getId());
		
		userDTO = modelMapper.map(user, UserDTO.class);
		return userDTO;
	}

	
}
