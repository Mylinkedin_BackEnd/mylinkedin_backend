package it.unisalento.sbapp.RestControllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.unisalento.sbapp.DTOClasses.OfferorDTO;
import it.unisalento.sbapp.DomainClasses.Offeror;
import it.unisalento.sbapp.Exception.SavingUserException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Iservice.IOfferorService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;



@RestController
@RequestMapping("/offeror")
public class OfferorRestController {
	
	@Autowired
	IOfferorService offerorService;
	
	
	private ModelMapper modelMapper = new ModelMapper();

//	CERCO UN OFFEROR DAL SUO ID
	@RequestMapping(value="/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public OfferorDTO get(@PathVariable int id  ) throws UserNotFoundException{

		Offeror offeror = offerorService.getById(id);
		
		// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO
		
		OfferorDTO offerorDTO = modelMapper.map(offeror, OfferorDTO.class);
		offerorDTO.setPassword("*******");

		
		
		return offerorDTO;
	}
	
//  CERCO TUTTI GLI OFFEROR
	@RequestMapping(value="/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<OfferorDTO> getAll( ){
		
		List<Offeror> 	offerorList = offerorService.getAll();
		List<OfferorDTO> offerorDTOList = new ArrayList<OfferorDTO>(); 
		
		for (Offeror offeror : offerorList) {
			
			OfferorDTO offerorDTO = modelMapper.map(offeror, OfferorDTO.class);

			offerorDTOList.add(offerorDTO);
		}
			
		
		return offerorDTOList;
	}


	
//  CERCO TUTTI GLI OFFEROR DISABILITATI {/true} O ABILITATI {/false}
	@RequestMapping(value="/getDisabled/{disable}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<OfferorDTO> getDisabled(@PathVariable boolean disable ) throws UserNotFoundException{
		
		List<Offeror> 	offerorList = offerorService.findByDisable(disable);
		List<OfferorDTO> offerorDTOList = new ArrayList<OfferorDTO>(); 
		
		for (Offeror offeror : offerorList) {
			
			OfferorDTO offerorDTO = modelMapper.map(offeror, OfferorDTO.class);

			offerorDTOList.add(offerorDTO);
		}
			
		
		return offerorDTOList;
	}
	
	
	// come passare parametri nel body della request
	// quando possiamo farlo ? solo in 1 caso : quando il method è un POST

//	AGGIUNGO UN NUOVO OFFEROR
	@PostMapping(value="/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public boolean save(@RequestBody @Valid OfferorDTO offerorDTO) throws SavingUserException {

		Offeror offeror  = modelMapper.map(offerorDTO, Offeror.class);
		offerorService.save(offeror);
		return true;
	}

//	RIMUOVO UN OFFEROR
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	public  ResponseEntity<Offeror> delete(@PathVariable int id ) throws UserNotFoundException,IllegalArgumentException{
		
		offerorService.delete(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
//  CERCO TUTTI GLI OFFEROR DI UNA AZIENDA
	@RequestMapping(value="/getByCompanyId/{companyId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<OfferorDTO> getByCompanyId(@PathVariable int companyId ) throws UserNotFoundException{
		
		List<Offeror> 	offerorList = offerorService.findByCompanyId(companyId);
		List<OfferorDTO> offerorDTOList = new ArrayList<OfferorDTO>(); 
		
		for (Offeror offeror : offerorList) {
			
			OfferorDTO offerorDTO = modelMapper.map(offeror, OfferorDTO.class);
			offerorDTO.setCompanyId(companyId);
			offerorDTO.setId(offeror.getId());
		
			offerorDTOList.add(offerorDTO);
		}
			
		
		return offerorDTOList;
	}
	
	@RequestMapping(value="/login/{username}/{password}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public OfferorDTO login(@PathVariable String username, @PathVariable String password ) throws UserNotFoundException{

		Offeror offeror = offerorService.findByUsernameAndPassword(username,password);
		
		// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO
		
		OfferorDTO offerorDTO = modelMapper.map(offeror, OfferorDTO.class);
		offerorDTO.setPassword("*******");
		
		return offerorDTO;
	}
	
	
}
