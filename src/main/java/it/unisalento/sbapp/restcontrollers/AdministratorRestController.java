
package it.unisalento.sbapp.RestControllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.unisalento.sbapp.DTOClasses.AdministratorDTO;
import it.unisalento.sbapp.DTOClasses.UserDTO;
import it.unisalento.sbapp.DomainClasses.Administrator;
import it.unisalento.sbapp.Exception.SavingUserException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Iservice.IAdministratorService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

@RestController
@RequestMapping("/administrator")
public class AdministratorRestController {

	@Autowired
	IAdministratorService administratorService;

	private ModelMapper modelMapper = new ModelMapper();


	/************** * Metodi di abilitazione dell'utente ********************/

	@PostMapping(value = "/enableUser", consumes = MediaType.APPLICATION_JSON_VALUE)
	public boolean EnableUser(@RequestBody UserDTO toManage) throws UserNotFoundException {
		return administratorService.EnableUserByUsername(toManage.getUsername());
	}

	@PostMapping(value = "/enableUserById", consumes = MediaType.APPLICATION_JSON_VALUE)
	public boolean EnableUserById(@RequestBody UserDTO toManage) throws UserNotFoundException {
		return administratorService.EnableUserById(toManage.getId());
	}

	/************************* Metodi di bloking dell'utente ****************/
	@PostMapping(value = "/lockUser", consumes = MediaType.APPLICATION_JSON_VALUE)
	public boolean lockUser(@RequestBody UserDTO toManage) throws UserNotFoundException {
		return administratorService.lockUser(toManage.getUsername());
	}

	@PostMapping(value = "/lockUserById", consumes = MediaType.APPLICATION_JSON_VALUE)
	public boolean lockUserById(@RequestBody UserDTO toManage ) throws UserNotFoundException {
		return administratorService.lockUserById(toManage.getId());
	}

	/********************** Metodi di unlock dell'utente *********************/

	@PostMapping(value = "/unlockUser", consumes = MediaType.APPLICATION_JSON_VALUE)
	public boolean unlockUser(@RequestBody UserDTO toManage) throws UserNotFoundException {
		return administratorService.unlockUser(toManage.getUsername());
	}

	@PostMapping(value = "/unlockUserById", consumes = MediaType.APPLICATION_JSON_VALUE)
	public boolean unlockUserById(@RequestBody UserDTO toManage) throws UserNotFoundException {
		return administratorService.unlockUserById(toManage.getId());
	}

	/**************************************************************************************/

//	RICERCO UN AMMINISTRATORE PER ID
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public AdministratorDTO get(@PathVariable int id) throws UserNotFoundException {

		Administrator administrator = administratorService.getById(id);

		// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO

		AdministratorDTO administratorDTO = modelMapper.map(administrator, AdministratorDTO.class);
		administratorDTO.setPassword("*******");

		return administratorDTO;
	}

	@RequestMapping(value = "/login/{username}/{password}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public AdministratorDTO login(@PathVariable String username, @PathVariable String password)
			throws UserNotFoundException {

		Administrator administrator = administratorService.findByUsernameAndPassword(username, password);

		// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO

		AdministratorDTO administratorDTO = modelMapper.map(administrator, AdministratorDTO.class);
		administratorDTO.setPassword("*******");

		return administratorDTO;
	}

//	RICERCO TUTTI GLI AMMINISTRATORI
	@RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AdministratorDTO> getAll() {

		List<Administrator> userList = administratorService.getAll();
		List<AdministratorDTO> userDTOList = new ArrayList<AdministratorDTO>();

		for (Administrator administrator : userList) {

			AdministratorDTO administratorDTO = modelMapper.map(administrator, AdministratorDTO.class);

			userDTOList.add(administratorDTO);
		}

		return userDTOList;
	}

	// come passare parametri nel body della request
	// quando possiamo farlo ? solo in 1 caso : quando il method è un POST

//	AGGIUNGO UN AMMINISTRATORE
	@PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public boolean save(@RequestBody @Valid AdministratorDTO administratorDTO) throws SavingUserException {

		Administrator administrator = modelMapper.map(administratorDTO, Administrator.class);

		administrator.setRegistrationDate(new Date());

		administratorService.save(administrator);
		return true;

	}

//	RIMUOVERE AMMINISTRATORE tramite il suo ID
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Administrator> delete(@PathVariable int id) throws UserNotFoundException,IllegalArgumentException {

		administratorService.delete(id);

		return new ResponseEntity<>(HttpStatus.OK);
	}

}