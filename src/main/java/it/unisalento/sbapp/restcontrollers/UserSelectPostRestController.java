package it.unisalento.sbapp.RestControllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.unisalento.sbapp.DTOClasses.UserSelectPostDTO;
import it.unisalento.sbapp.DTOClasses.UserSignalPostDTO;
import it.unisalento.sbapp.DomainClasses.UserSelectPost;
import it.unisalento.sbapp.Exception.PostNotFoundException;
import it.unisalento.sbapp.Exception.SavingUserSelectPostException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Exception.UserSelectPostNotFoundException;
import it.unisalento.sbapp.Iservice.IPostService;
import it.unisalento.sbapp.Iservice.IUserService;
import it.unisalento.sbapp.Iservice.IuserSelectPostService;

import java.util.ArrayList;
import java.util.List;

import javax.print.attribute.standard.Media;
import javax.validation.Valid;



@RestController
@RequestMapping("/userSelectPost")
public class UserSelectPostRestController {
	
	@Autowired
	IuserSelectPostService userSelectPostService;
	
	@Autowired
	IUserService userService;
	
	@Autowired
	IPostService postService;
	
	
	private ModelMapper modelMapper = new ModelMapper();


	
	
	
	@RequestMapping(value="/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public UserSelectPostDTO get(@PathVariable int id) throws UserSelectPostNotFoundException{
		
		UserSelectPost userSelectPost = userSelectPostService.getById(id);
		
		// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO
		
		UserSelectPostDTO userSelectPostDTO = modelMapper.map(userSelectPost, UserSelectPostDTO.class);
		
		
		return userSelectPostDTO;
	}
	
	
	@RequestMapping(value="/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserSelectPostDTO> getAll(){
		
		List<UserSelectPost> 	userSelectPostsList = userSelectPostService.getAll();
		List<UserSelectPostDTO> userSelectPostDTOList = new ArrayList<UserSelectPostDTO>(); 
		
		for (UserSelectPost userSelectPost : userSelectPostsList) {
			
			UserSelectPostDTO userSelectPostDTO = modelMapper.map(userSelectPost, UserSelectPostDTO.class);
			
			
			userSelectPostDTOList.add(userSelectPostDTO);
		}
		
		
		return userSelectPostDTOList;
	}
	
//	CERCO TUTTE LE RELAZIONI {UTENTE SELEZIONA POST} DI UN PARTICOLARE UTENTE DAL SUO ID
//	*****UTILE PER TROVARE LE OFFERTE CHE HO GIA' SELEZIONATO****
	@RequestMapping(value="/getUserSelectPostByUserId/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserSelectPostDTO> getUserSelectPostByUserId(@PathVariable int userId) throws UserNotFoundException{
		
		List<UserSelectPost> 	userSelectPostsList = userSelectPostService.findByUserId(userId);
		List<UserSelectPostDTO> userSelectPostDTOList = new ArrayList<UserSelectPostDTO>(); 
		
		for (UserSelectPost userSelectPost : userSelectPostsList) {
			
			UserSelectPostDTO userSelectPostDTO = modelMapper.map(userSelectPost, UserSelectPostDTO.class);
			userSelectPostDTO.setPost_id(userSelectPost.getPost().getId());
			userSelectPostDTO.setUser_id(userSelectPost.getUser().getId());
			
			userSelectPostDTOList.add(userSelectPostDTO);
		}
		
		return userSelectPostDTOList;
	}
	
//	CERCO TUTTE LE RELAZIONI {UTENTE SELEZIONA POST} DI UN PARTICOLARE UTENTE DAL SUO USERNAME
//	*****UTILE PER TROVARE LE OFFERTE CHE HO GIA' SELEZIONATO****	
	@RequestMapping(value="/getUserSelectPostByUserUsername/{userUsername}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserSelectPostDTO> getUserSelectPostByUserUsername(@PathVariable String userUsername) throws UserNotFoundException{
		
		List<UserSelectPost> 	userSelectPostsList = userSelectPostService.findByUserUsername(userUsername);
		List<UserSelectPostDTO> userSelectPostDTOList = new ArrayList<UserSelectPostDTO>(); 
		
		for (UserSelectPost userSelectPost : userSelectPostsList) {
			
			UserSelectPostDTO userSelectPostDTO = modelMapper.map(userSelectPost, UserSelectPostDTO.class);
			userSelectPostDTO.setPost_id(userSelectPost.getPost().getId());
			userSelectPostDTO.setUser_id(userSelectPost.getUser().getId());
			
			userSelectPostDTOList.add(userSelectPostDTO);
		}
		
		return userSelectPostDTOList;
	}

//	CERCO TUTTE LE RELAZIONI {UTENTE SELEZIONA POST} DI UN PARTICOLARE POST, QUINDI POSSO RISALIRE A TUTTI GLI UTENTI CHE SELEZIONANO UN POST
//	*****UTILE PER TROVARE LE OFFERTE CHE RISPETTANO IL CASO D'USO : BULK NOTIFICATION****
	@RequestMapping(value="/getUserSelectPostByPostId/{postId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserSelectPostDTO> getUserSelectPostByPostId(@PathVariable int postId) throws UserNotFoundException, PostNotFoundException{
		
		List<UserSelectPost> 	userSelectPostsList = userSelectPostService.findByPostId(postId);
		List<UserSelectPostDTO> userSelectPostDTOList = new ArrayList<UserSelectPostDTO>(); 
		
		for (UserSelectPost userSelectPost : userSelectPostsList) {
			
			UserSelectPostDTO userSelectPostDTO = modelMapper.map(userSelectPost, UserSelectPostDTO.class);
			userSelectPostDTO.setPost_id(userSelectPost.getPost().getId());
			userSelectPostDTO.setUser_id(userSelectPost.getUser().getId());
			
			userSelectPostDTOList.add(userSelectPostDTO);
		}
		
		return userSelectPostDTOList;
	}
	
//	AGGIUNGO UNA RELAZIONE
	@PostMapping(value="/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public UserSelectPostDTO save(@RequestBody @Valid UserSelectPostDTO userSelectPostDTO) throws SavingUserSelectPostException, UserNotFoundException, PostNotFoundException {
		
		UserSelectPost userSelectPost = modelMapper.map(userSelectPostDTO, UserSelectPost.class);
		
		userSelectPost.setPost(postService.getById(userSelectPostDTO.getPost_id()));
		userSelectPost.setUser(userService.getById(userSelectPostDTO.getUser_id()));

		UserSelectPost userSelectPostSaved= userSelectPostService.save(userSelectPost);	
		userSelectPostDTO.setId(userSelectPostSaved.getId());
		
		return userSelectPostDTO;
		
	}
	
//	RIMUOVO UNA RELAZIONE
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	public  ResponseEntity<UserSelectPost> delete(@PathVariable int id ) throws UserSelectPostNotFoundException,IllegalArgumentException{
		
		userSelectPostService.delete(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
}
