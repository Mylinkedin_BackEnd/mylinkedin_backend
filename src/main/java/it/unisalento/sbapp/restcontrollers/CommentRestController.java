package it.unisalento.sbapp.RestControllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import it.unisalento.sbapp.DTOClasses.CommentDTO;
import it.unisalento.sbapp.DomainClasses.Comment;
import it.unisalento.sbapp.Exception.CommentNotFoundException;
import it.unisalento.sbapp.Exception.PostNotFoundException;
import it.unisalento.sbapp.Exception.SavingCommentException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Iservice.*;


@RestController
@RequestMapping("/comment")
public class CommentRestController {
	
	@Autowired
	ICommentService commentService;
	
	@Autowired
	IPostService postService;
	
	@Autowired
	IUserService userService;
	
	private ModelMapper modelMapper = new ModelMapper();

// CERCO UN COMMENTO DALL'ID	
	@RequestMapping(value="/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public CommentDTO get(@PathVariable int id) throws CommentNotFoundException{
		
		Comment comment = commentService.getById(id);
		
		// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO
		
		CommentDTO commentDTO = modelMapper.map(comment, CommentDTO.class);
		
		
//		commentDTO.setId(comment.getId());
//		commentDTO.setInsertionDate(comment.getInsertionDate());
//		commentDTO.setComment_id(comment.getComment().getId()); // bisogna settare il comment_id == id se non è un sottocommento
//		commentDTO.setPost_id(comment.getPost().getId());
//		commentDTO.setText(comment.getText());
//		commentDTO.setUser_id(comment.getUser().getId());

		
		return commentDTO;
	}
	
// CERCO TUTTI I COMMENTI	
	@RequestMapping(value="/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CommentDTO> getAll(){
		
		List<Comment> 	commentList = commentService.getAll();
		List<CommentDTO> commentDTOList = new ArrayList<CommentDTO>(); 
		
		for (Comment comment : commentList) {
			
			CommentDTO commentDTO = modelMapper.map(comment, CommentDTO.class);
			
//			commentDTO.setId(comment.getId());
//			commentDTO.setInsertionDate(comment.getInsertionDate());
//			commentDTO.setComment_id(comment.getComment().getId());
//			commentDTO.setPost_id(comment.getPost().getId());
//			commentDTO.setText(comment.getText());
//			commentDTO.setUser_id(comment.getUser().getId());
//			
			commentDTOList.add(commentDTO);
		}
		
		
		return commentDTOList;
	}

//	CERCO TUTTI I COMMENTI DI UN POST dall' ID del POST
	@RequestMapping(value="/getByPostId/{postId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CommentDTO> getByPostId(@PathVariable int postID) throws PostNotFoundException{
		
		List<Comment> 	commentList = commentService.getByPostId(postID);
		List<CommentDTO> commentDTOList = new ArrayList<CommentDTO>(); 
		
		for (Comment comment : commentList) {
			
			CommentDTO commentDTO = modelMapper.map(comment, CommentDTO.class);

			commentDTOList.add(commentDTO);
		}
		
		
		return commentDTOList;
	}

// CERCO TUTTI I SOTTOCOMMENTI DI UN COMMENTO DALL' ID del COMMENTO	
	@RequestMapping(value="/getByCommentId/{commentId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CommentDTO> getByCommentId(@PathVariable int commentID) throws CommentNotFoundException{
		
		List<Comment> 	commentList = commentService.getByCommentId(commentID);
		List<CommentDTO> commentDTOList = new ArrayList<CommentDTO>(); 
		
		for (Comment comment : commentList) {
			
			CommentDTO commentDTO = modelMapper.map(comment, CommentDTO.class);
			commentDTOList.add(commentDTO);
		}
		
		
		return commentDTOList;
	}
	
	
//  SALVO UN COMMENTO
	@PostMapping(value="/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public CommentDTO save(@RequestBody @Valid CommentDTO commentDTO) throws SavingCommentException, CommentNotFoundException, PostNotFoundException, UserNotFoundException {
		
		Comment comment = modelMapper.map(commentDTO, Comment.class);

		
		comment.setPost(postService.getById(commentDTO.getPost_id()));
		comment.setUser(userService.getById(commentDTO.getUser_id()));
	
		comment.setText(commentDTO.getText());
		
		if (commentDTO.getComment_id() != 0)
				comment.setComment(commentService.getById(commentDTO.getComment_id()));
		else {// se il commento crea una nuova thread, associo il suo stesso id anche come comment_id
				comment.setComment(commentService.getById(commentDTO.getId())); 
		}
		
		Date date = new Date();
		comment.setInsertionDate(date);
		
		Comment commentSaved = commentService.save(comment);	
		commentDTO.setId(commentSaved.getId());
		
		return commentDTO;
		
	}
	
// RIMUOVO UN COMMENTO	
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	public  ResponseEntity<Comment> delete(@PathVariable int id ) throws CommentNotFoundException, IllegalArgumentException{
		
		commentService.delete(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
}
