package it.unisalento.sbapp.RestControllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.unisalento.sbapp.DTOClasses.CompanyDTO;
import it.unisalento.sbapp.DomainClasses.Company;
import it.unisalento.sbapp.Exception.CompanyNotFoundException;
import it.unisalento.sbapp.Exception.SavingCompanyException;
import it.unisalento.sbapp.Iservice.*;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

@RestController
@RequestMapping("/company")
public class CompanyRestController {
	
	@Autowired
	ICompanyService companyService;
	
	private ModelMapper modelMapper = new ModelMapper();

//	CERCO UN'AZIENDA DALL'ID
	@RequestMapping(value="/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public CompanyDTO get(@PathVariable int id) throws CompanyNotFoundException{
		
		Company company = companyService.getById(id);
		
		CompanyDTO companyDTO = modelMapper.map(company, CompanyDTO.class);
		
		return companyDTO;
	}
	
//	CERCO TUTTE LE AZIENDE
	@RequestMapping(value="/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CompanyDTO> getAll(){
		
		List<Company> 	companyList = companyService.getAll();
		List<CompanyDTO> companyDTOList = new ArrayList<CompanyDTO>(); 
		
		for (Company company : companyList) {
			
			CompanyDTO companyDTO = modelMapper.map(company, CompanyDTO.class);
			
			companyDTOList.add(companyDTO);
	
		}


		return companyDTOList;
	}

	
// SALVO UNA NUOVA AZIENDA	
	@PostMapping(value="/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public CompanyDTO save(@RequestBody @Valid CompanyDTO companyDTO) throws CompanyNotFoundException, SavingCompanyException {
		
		
		Company company = modelMapper.map(companyDTO, Company.class);		
		Company companySaved = companyService.save(company);	
		companyDTO.setId(companySaved.getId());
		
		return companyDTO;
		
	}

// RIMUOVO UN'AZIENDA
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	public  ResponseEntity<Company> delete(@PathVariable int id ) throws CompanyNotFoundException, IllegalArgumentException{
		
		companyService.delete(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
// CERCO UN'AZIENDA DAL NOME
	@RequestMapping(value="/getByName/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public CompanyDTO getByName(@PathVariable String name) throws CompanyNotFoundException{
		
		Company company = companyService.getByName(name);
	
		CompanyDTO companyDTO = modelMapper.map(company, CompanyDTO.class);
		
		return companyDTO;
	}

	// CERCO UN'AZIENDA DAL NOME
		@RequestMapping(value="/getByName/{IVA}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public CompanyDTO getByIVA(@PathVariable String IVA) throws CompanyNotFoundException{
			
			Company company = companyService.findByIva(IVA);
		
			CompanyDTO companyDTO = modelMapper.map(company, CompanyDTO.class);
			
			return companyDTO;
		}
}
