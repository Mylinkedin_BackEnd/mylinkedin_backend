package it.unisalento.sbapp.RestControllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import it.unisalento.sbapp.DTOClasses.AttributeStructureDTO;
import it.unisalento.sbapp.DTOClasses.StructureDTO;
import it.unisalento.sbapp.DomainClasses.Attribute;
import it.unisalento.sbapp.DomainClasses.Structure;
import it.unisalento.sbapp.Exception.SavingStructureException;
import it.unisalento.sbapp.Exception.StructureNotFoundException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Iservice.IAttributeStructureService;
import it.unisalento.sbapp.Iservice.IStructureService;
import it.unisalento.sbapp.Iservice.IUserService;

@RestController
@RequestMapping("/structure")
public class StructureRestController {
	
	@Autowired
	IStructureService structureService;
	
	@Autowired
	IUserService userService;
	
	@Autowired
	IAttributeStructureService attributeStructureService;
	
	private ModelMapper modelMapper = new ModelMapper();

	
	@RequestMapping(value="/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public StructureDTO get(@PathVariable int id) throws StructureNotFoundException{
		
		Structure structure = structureService.getById(id);
		
		// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO
		
		StructureDTO structureDTO = modelMapper.map(structure, StructureDTO.class);
		
//		structureDTO.setId(structure.getId());
//		structureDTO.setTitle(structure.getTitle());
//		structureDTO.setDescription(structure.getDescription());
//		structureDTO.setUser_id(structure.getUser().getId());
		
		return structureDTO;
	}
	
	
	@RequestMapping(value="/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<StructureDTO> getAll(){
		
		List<Structure> 	structureList = structureService.getAll();
		List<StructureDTO> structureDTOList = new ArrayList<StructureDTO>(); 
		
		for (Structure structure : structureList) {
			
			StructureDTO structureDTO = modelMapper.map(structure, StructureDTO.class);
			
//			structureDTO.setId(structure.getId());
//			structureDTO.setTitle(structure.getTitle());
//			structureDTO.setDescription(structure.getDescription());
//			structureDTO.setUser_id(structure.getUser().getId());
			
			structureDTOList.add(structureDTO);
		}
		
		
		return structureDTOList;
	}

	
	
	@PostMapping(value="/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public StructureDTO save(@RequestBody @Valid StructureDTO structureDTO) throws SavingStructureException, UserNotFoundException {
		
		Structure structure = modelMapper.map(structureDTO, Structure.class);

		Structure structureSaved = structureService.save(structure);	
		structureDTO.setId(structureSaved.getId());
		
		return structureDTO;
		
	}
	
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	public  ResponseEntity<Structure> delete(@PathVariable int id ) throws StructureNotFoundException,IllegalArgumentException{
		
		structureService.delete(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}

//	CERCO UNA STRUTTURA DAL NOME
	@RequestMapping(value="/getByTitle/{title}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public StructureDTO get(@PathVariable String title) throws StructureNotFoundException{
		
		try {
			Structure structure = structureService.findByTitle(title);
			StructureDTO structureDTO = modelMapper.map(structure, StructureDTO.class);
			return structureDTO;

		} catch (Exception e) {
			throw new StructureNotFoundException();
		}
		
		// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO
		

		
	}
	

	@PostMapping(value = "/newStructureWithAttribute", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void newStructureWithAttribute(@RequestBody List<AttributeStructureDTO> bounds) {

		Structure structure = modelMapper.map(bounds.get(0).getStructure_id(), Structure.class);
		for (AttributeStructureDTO bound : bounds) {
			Attribute attr = modelMapper.map(bound.getAttribute_id(), Attribute.class);
			attributeStructureService.createBound(structure, attr);

		}

	}
	
}
