package it.unisalento.sbapp.RestControllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import it.unisalento.sbapp.DTOClasses.AttributeDTO;
import it.unisalento.sbapp.DomainClasses.Attribute;
import it.unisalento.sbapp.DomainClasses.AttributeStructure;
import it.unisalento.sbapp.Exception.AttributeNotFoundException;
import it.unisalento.sbapp.Exception.AttributeStructureNotFound;
import it.unisalento.sbapp.Exception.SavingAttributeException;
import it.unisalento.sbapp.Iservice.*;



@RestController
@RequestMapping("/attribute")
public class AttributeRestController {
	
	@Autowired
	IAttributeService attributeService;
	
	@Autowired
	IAttributeStructureService attributeStructureService;
	
	private ModelMapper modelMapper = new ModelMapper();

//	GLI ATTIRBUTI SARANNO MASSIMO 15-20 PER TUTTO IL PROGETTO
//	RICERO UN ATTRIBUTO secondo il suo ID
	@RequestMapping(value="/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public AttributeDTO get(@PathVariable int id) throws AttributeNotFoundException{
		
		Attribute attribute = attributeService.getById(id);
		
		// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO
		
		AttributeDTO attributeDTO = modelMapper.map(attribute, AttributeDTO.class);
		
//		attributeDTO.setId(attribute.getId());
//		attributeDTO.setName(attribute.getName());
//		attributeDTO.setType(attribute.getType());
//		attributeDTO.setValue(attribute.getValue());
		
		return attributeDTO;
	}
	
//	RICERCO ATTRIBUTE per NOME
	@RequestMapping(value="/getByName/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public AttributeDTO getByName(@PathVariable String name) throws AttributeNotFoundException{
		
		Attribute attribute = attributeService.getByNameLike(name);
		AttributeDTO attributeDTO = modelMapper.map(attribute, AttributeDTO.class);

		return attributeDTO;
	}
	
//	RICERCO TUTTI GLI ATTRIBUTE
	@RequestMapping(value="/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AttributeDTO> getAll(){
		
		List<Attribute> 	attributeList = attributeService.getAll();
		List<AttributeDTO> attributrDTOList = new ArrayList<AttributeDTO>(); 
		
		for (Attribute attribute : attributeList) {
			
			AttributeDTO attributeDTO = modelMapper.map(attribute, AttributeDTO.class);
			
//			attributeDTO.setId(attribute.getId());
//			attributeDTO.setName(attribute.getName());
//			attributeDTO.setType(attribute.getType());
//			attributeDTO.setValue(attribute.getValue());

			
			attributrDTOList.add(attributeDTO);
		}
		
		
		return attributrDTOList;
	}

	
//	SALVO UN ATTRIBUTE
	@PostMapping(value="/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public AttributeDTO save(@RequestBody @Valid AttributeDTO attributeDTO) throws SavingAttributeException {
		
		Attribute attribute = modelMapper.map(attributeDTO, Attribute.class);
	
//		attribute.setName(attributeDTO.getName());
//		attribute.setType(attributeDTO.getType());
//		attribute.setValue(attributeDTO.getValue());

		
		Attribute attributeSaved = attributeService.save(attribute);	
		attributeDTO.setId(attributeSaved.getId());
		
		return attributeDTO;
		
	}
//	RIMUOVO UN ATTRIBUTE
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	public  ResponseEntity<Attribute> delete(@PathVariable int id ) throws AttributeNotFoundException,IllegalArgumentException{
		
		attributeService.delete(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(value="/getAttributeByStructureId/{structureId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AttributeDTO> getAttributeByStructureId(@PathVariable int structureId) throws AttributeNotFoundException, AttributeStructureNotFound{
		
		List<AttributeStructure> 	attributeStructureList = attributeStructureService.findByStructureId(structureId);
		List<AttributeDTO> attributeDTOList = new ArrayList<AttributeDTO>();
		
		for (AttributeStructure attributeStructure : attributeStructureList) {
			
			Attribute attribute = attributeService.getById(attributeStructure.getAttribute().getId());
			AttributeDTO attributeDTO = modelMapper.map(attribute, AttributeDTO.class);

			attributeDTOList.add(attributeDTO);
		}
		
		return attributeDTOList;
	}
	
//	CERCO TUTTI GLI ATTRIBUTE ASSOCIATI AD UNA STRUTTURA (attraverso il NOME della struttura)
	@RequestMapping(value="/getAttributeByStructureTitle/{structureTitle}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AttributeDTO> getAttributeByStructureTitle(@PathVariable String structureTitle) throws AttributeNotFoundException, AttributeStructureNotFound{
		
		List<AttributeStructure> 	attributeStructureList = attributeStructureService.findByStructureTitle(structureTitle);
		List<AttributeDTO> attributeDTOList = new ArrayList<AttributeDTO>();
		
		for (AttributeStructure attributeStructure : attributeStructureList) {
			
			Attribute attribute = attributeService.getById(attributeStructure.getAttribute().getId());
			AttributeDTO attributeDTO = modelMapper.map(attribute, AttributeDTO.class);

			attributeDTOList.add(attributeDTO);
		}
		
		return attributeDTOList;
	}
	
	

}
