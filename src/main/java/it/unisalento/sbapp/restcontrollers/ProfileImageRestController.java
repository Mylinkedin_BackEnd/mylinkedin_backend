package it.unisalento.sbapp.RestControllers;

	import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.http.HttpStatus;
	import org.springframework.http.MediaType;
	import org.springframework.http.ResponseEntity;
	import org.springframework.web.bind.annotation.PathVariable;
	import org.springframework.web.bind.annotation.PostMapping;
	import org.springframework.web.bind.annotation.RequestBody;
	import org.springframework.web.bind.annotation.RequestMapping;
	import org.springframework.web.bind.annotation.RequestMethod;
	import org.springframework.web.bind.annotation.RestController;

	import java.util.ArrayList;
	import java.util.List;

	import javax.validation.Valid;

import it.unisalento.sbapp.DTOClasses.ProfileImageDTO;
import it.unisalento.sbapp.DomainClasses.ProfileImage;
import it.unisalento.sbapp.Exception.ProfileImageNotFoundException;
import it.unisalento.sbapp.Exception.SavingProfileImageException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Iservice.IProfileImageService;
import it.unisalento.sbapp.Iservice.IUserService;


	@RestController
	@RequestMapping("/proflieImage")
	public class ProfileImageRestController {
		
		@Autowired
		IProfileImageService profileImageService;

		@Autowired
		IUserService userService;
		
		private ModelMapper modelMapper = new ModelMapper();


		@RequestMapping(value="/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public ProfileImageDTO get(@PathVariable int id) throws ProfileImageNotFoundException{
			
			ProfileImage profileImage = profileImageService.getById(id);
			
			// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO
			
			ProfileImageDTO profileImageDTO = modelMapper.map(profileImage, ProfileImageDTO.class);
			
//			profileImageDTO.setId(profileImage.getId());
//			profileImageDTO.setDescription(profileImage.getDescription());
//			profileImageDTO.setUser_id(profileImage.getUser().getId());
		
			
			return profileImageDTO;
		}
		
		
		@RequestMapping(value="/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public List<ProfileImageDTO> getAll(){
			
			List<ProfileImage> 	profileImageList = profileImageService.getAll();
			List<ProfileImageDTO> profileImageDTOList = new ArrayList<ProfileImageDTO>(); 
			
			for (ProfileImage profileImage : profileImageList) {
				
				ProfileImageDTO profileImageDTO = modelMapper.map(profileImage, ProfileImageDTO.class);
				
//				profileImageDTO.setId(profileImage.getId());
//				profileImageDTO.setDescription(profileImage.getDescription());
//				profileImageDTO.setUser_id(profileImage.getUser().getId());
				
				profileImageDTOList.add(profileImageDTO);
			}
			
			
			return profileImageDTOList;
		}

		
		
		@PostMapping(value="/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
		public ProfileImageDTO save(@RequestBody @Valid ProfileImageDTO profileImageDTO) throws SavingProfileImageException, UserNotFoundException {
			
			ProfileImage profileImage = modelMapper.map(profileImageDTO, ProfileImage.class);
			
//			profileImage.setDescription(profileImageDTO.getDescription());
			profileImage.setUser(userService.getById(profileImageDTO.getUser_id()));
			
			ProfileImage profileImageSaved = profileImageService.save(profileImage);	
			profileImageDTO.setId(profileImageSaved.getId());
			
			return profileImageDTO;
			
		}
		
		@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
		public  ResponseEntity<ProfileImage> delete(@PathVariable int id ) throws ProfileImageNotFoundException,IllegalArgumentException{
			
			profileImageService.delete(id);
			
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
//		CERCO UN'IMMAGINE DALL'ID DI UNO USER
		@RequestMapping(value="/getByUserId/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public ProfileImageDTO getByUserId(@PathVariable int userId) throws ProfileImageNotFoundException, UserNotFoundException{
			
			ProfileImage profileImage = profileImageService.findByUserId(userId);
			ProfileImageDTO profileImageDTO = modelMapper.map(profileImage, ProfileImageDTO.class);
			
			
			return profileImageDTO;
		}

//		CERCO UN'IMMAGINE DALLO USERNAME DI UNO USER		
		@RequestMapping(value="/getByUserUsername/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public ProfileImageDTO getByUserUsername(@PathVariable String username) throws ProfileImageNotFoundException, UserNotFoundException{
			
			ProfileImage profileImage = profileImageService.findByUserUsername(username);
			ProfileImageDTO profileImageDTO = modelMapper.map(profileImage, ProfileImageDTO.class);
			
			
			return profileImageDTO;
		}
	}
