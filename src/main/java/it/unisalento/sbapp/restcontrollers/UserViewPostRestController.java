package it.unisalento.sbapp.RestControllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.unisalento.sbapp.DTOClasses.UserViewPostDTO;
import it.unisalento.sbapp.DomainClasses.UserViewPost;
import it.unisalento.sbapp.Exception.PostNotFoundException;
import it.unisalento.sbapp.Exception.SavingUserViewPostException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Exception.UserViewPostNotFoundException;
import it.unisalento.sbapp.Iservice.IPostService;
import it.unisalento.sbapp.Iservice.IUserService;
import it.unisalento.sbapp.Iservice.IUserViewPostService;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;



@RestController
@RequestMapping("/userViewPost")
public class UserViewPostRestController {
	
	@Autowired
	IUserViewPostService userViewPostService;
	
	@Autowired
	IUserService userService;
	
	@Autowired
	IPostService postService;
	
	
	private ModelMapper modelMapper = new ModelMapper();

	
	@RequestMapping(value="/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public UserViewPostDTO get(@PathVariable int id) throws UserViewPostNotFoundException{
		
		UserViewPost userViewPost = userViewPostService.getById(id);
		
		// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO
		
		UserViewPostDTO userViewPostDTO = modelMapper.map(userViewPost, UserViewPostDTO.class);
		
		
		return userViewPostDTO;
	}
	
	
	@RequestMapping(value="/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserViewPostDTO> getAll(){
		
		List<UserViewPost> 	userViewPostsList = userViewPostService.getAll();
		List<UserViewPostDTO> userViewPostDTOList = new ArrayList<UserViewPostDTO>(); 
		
		for (UserViewPost userViewPost : userViewPostsList) {
			
			UserViewPostDTO userViewPostDTO = modelMapper.map(userViewPost, UserViewPostDTO.class);
			
			
			userViewPostDTOList.add(userViewPostDTO);
		}
		
		
		return userViewPostDTOList;
	}

	
	
	@PostMapping(value="/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public UserViewPostDTO save(@RequestBody @Valid UserViewPostDTO userViewPostDTO) throws SavingUserViewPostException, UserNotFoundException, PostNotFoundException {
		
		UserViewPost userViewPost = modelMapper.map(userViewPostDTO, UserViewPost.class);
		
		userViewPost.setPost(postService.getById(userViewPostDTO.getPost_id()));
		userViewPost.setUser(userService.getById(userViewPostDTO.getUser_id()));

		UserViewPost userViewPostSaved= userViewPostService.save(userViewPost);	
		userViewPostDTO.setId(userViewPostSaved.getId());
		
		return userViewPostDTO;
		
	}
	
//	CERCO TUTTE LE RELAZIONI {USER VEDE POST} DI UN POST (DAL SUO ID)
	@RequestMapping(value="/getUserViewPostByPostId/{postId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserViewPostDTO> getUserViewPostByPostId(@PathVariable int postId) throws UserViewPostNotFoundException{
		
		List<UserViewPost> 	userviewPostsList = userViewPostService.findByPostId(postId);
		List<UserViewPostDTO> userviewPostDTOList = new ArrayList<UserViewPostDTO>(); 
		
		for (UserViewPost userViewPost : userviewPostsList) {
			
			UserViewPostDTO userviewPostDTO = modelMapper.map(userViewPost, UserViewPostDTO.class);
			userviewPostDTO.setPost_id(userViewPost.getPost().getId());
			userviewPostDTO.setUser_id(userViewPost.getUser().getId());
			
			userviewPostDTOList.add(userviewPostDTO);
		}
		
		return userviewPostDTOList;
	}
	
//	CERCO TUTTE LE RELAZIONI {USER VEDE POST} DELLO USER(DAL SUO ID)
	@RequestMapping(value="/getUserViewPostByUserId/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserViewPostDTO> getUserViewPostByUserId(@PathVariable int userId) throws UserViewPostNotFoundException{
		
		List<UserViewPost> 	userviewPostsList = userViewPostService.findByUserId(userId);
		List<UserViewPostDTO> userviewPostDTOList = new ArrayList<UserViewPostDTO>(); 
		
		for (UserViewPost userViewPost : userviewPostsList) {
			
			UserViewPostDTO userviewPostDTO = modelMapper.map(userViewPost, UserViewPostDTO.class);
			userviewPostDTO.setPost_id(userViewPost.getPost().getId());
			userviewPostDTO.setUser_id(userViewPost.getUser().getId());
			
			userviewPostDTOList.add(userviewPostDTO);
		}
		
		return userviewPostDTOList;
	}
	
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	public  boolean delete(@PathVariable int id ) throws UserViewPostNotFoundException,IllegalArgumentException{
				
		return userViewPostService.delete(id);
	}
	
}
