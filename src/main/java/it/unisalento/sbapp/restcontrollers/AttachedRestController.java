package it.unisalento.sbapp.RestControllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import it.unisalento.sbapp.DTOClasses.AttachedDTO;
import it.unisalento.sbapp.DomainClasses.Attached;
import it.unisalento.sbapp.Exception.AttachedNotFoundException;
import it.unisalento.sbapp.Exception.PostNotFoundException;
import it.unisalento.sbapp.Exception.SavingAttachedException;
import it.unisalento.sbapp.Iservice.IAttachedService;
import it.unisalento.sbapp.Iservice.IPostService;


@RestController
@RequestMapping("/attached")
public class AttachedRestController {
	
	@Autowired
	IAttachedService attachedService;
	
	@Autowired
	IPostService postService;
	
	private ModelMapper modelMapper = new ModelMapper();

//	RICERCO UN ALLEGATO PER ID
	@RequestMapping(value="/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public AttachedDTO get(@PathVariable int id) throws AttachedNotFoundException{
		
		Attached attached = attachedService.getById(id);
		
		// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO
		
		AttachedDTO attachedDTO = modelMapper.map(attached, AttachedDTO.class);
		
//		attachedDTO.setId(attached.getId());
//		attachedDTO.setFormat(attached.getFormat());
//		attachedDTO.setName(attached.getName());
//		attachedDTO.setPost_id(attached.getPost().getId());
		
		return attachedDTO;
	}
	
	
//	RICERCO TUTTI GLI ALLEGATI
	@RequestMapping(value="/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AttachedDTO> getAll(){
		
		List<Attached> 	attachedList = attachedService.getAll();
		List<AttachedDTO> attachedDTOList = new ArrayList<AttachedDTO>(); 
		
		for (Attached attached : attachedList) {
			
			AttachedDTO attachedDTO = modelMapper.map(attached, AttachedDTO.class);
			
//			attachedDTO.setId(attached.getId());
//			attachedDTO.setFormat(attached.getFormat());
//			attachedDTO.setName(attached.getName());
//			attachedDTO.setPost_id(attached.getPost().getId());
			
			attachedDTOList.add(attachedDTO);
		}
		
		
		return attachedDTOList;
	}

	
//	SALVO UN ALLEGATO
	@PostMapping(value="/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public AttachedDTO save(@RequestBody @Valid AttachedDTO attachedDTO) throws SavingAttachedException, PostNotFoundException {
		
		
		Attached attached = modelMapper.map(attachedDTO, Attached.class);

		attached.setPost(postService.getById(attachedDTO.getPost_id()));
		
		Attached attachedSaved = attachedService.save(attached);	
		attachedDTO.setId(attachedSaved.getId());
		
		return attachedDTO;
		
	}
	
//	RIMUOVO UN ALLEGATO
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	public  ResponseEntity<Attached> delete(@PathVariable int id ) throws AttachedNotFoundException{
		
		attachedService.delete(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	

//	RICERCO TUTTI GLI ALLEGATI ASSOCIATI AD UN POST
	@RequestMapping(value="/getByPostId/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AttachedDTO> getByPostId(@PathVariable int id) throws PostNotFoundException{
		
		List<Attached> 	attachedList = attachedService.findByPostId(id);
		List<AttachedDTO> attachedDTOList = new ArrayList<AttachedDTO>(); 
		
		for (Attached attached : attachedList) {
			
			AttachedDTO attachedDTO = modelMapper.map(attached, AttachedDTO.class);
			attachedDTOList.add(attachedDTO);
		}
		
		
		return attachedDTOList;
	}

}
