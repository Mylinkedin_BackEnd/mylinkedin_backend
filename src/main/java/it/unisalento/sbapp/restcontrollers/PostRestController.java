package it.unisalento.sbapp.RestControllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import it.unisalento.sbapp.DTOClasses.AttributeDTO;
import it.unisalento.sbapp.DTOClasses.PostDTO;
import it.unisalento.sbapp.DomainClasses.Attribute;
import it.unisalento.sbapp.DomainClasses.AttributeStructure;
import it.unisalento.sbapp.DomainClasses.Post;
import it.unisalento.sbapp.DomainClasses.UserSelectPost;
import it.unisalento.sbapp.DomainClasses.UserViewPost;
import it.unisalento.sbapp.Exception.AttributeNotFoundException;
import it.unisalento.sbapp.Exception.PostNotFoundException;
import it.unisalento.sbapp.Exception.SavingPostException;
import it.unisalento.sbapp.Exception.StructureNotFoundException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Exception.UserViewPostNotFoundException;
import it.unisalento.sbapp.Iservice.*;
 
@RestController
@RequestMapping("/post")
public class PostRestController {
	
	@Autowired
	IPostService postService;
	
	@Autowired
	IStructureService structureService;
	
	@Autowired
	IUserService userService;
	
	@Autowired
	IAttributeService attributeService;
	
	@Autowired
	IuserSelectPostService userSelectPostService;
	
	@Autowired
	IUserViewPostService userViewPostService;
	
	private ModelMapper modelMapper = new ModelMapper();

//	CERCO UN POST DAL SUO ID
	@RequestMapping(value="/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public PostDTO get(@PathVariable int id) throws PostNotFoundException{
		
		Post post = postService.getById(id);
		
		// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO
		
		PostDTO postDTO = modelMapper.map(post, PostDTO.class);

		
//		postDTO.setHide(post.isHide());
//		postDTO.setPubblicationDate(post.getPubblicationDate());
//		postDTO.setText(post.getText());
//		postDTO.setStructure_id(post.getStructure().getId());
//		postDTO.setUser_id(post.getUser().getId());
//		postDTO.setId(post.getId());
		
		return postDTO;
	}
	

//	CERCO TUTTI I POST
	@RequestMapping(value="/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PostDTO> getAll(){
		
		
		List<Post> 	postList = postService.getAll();
		List<PostDTO> postDTOList = new ArrayList<PostDTO>(); 
		
		for (Post post : postList) {
			
			PostDTO postDTO = modelMapper.map(post, PostDTO.class);
			
			postDTOList.add(postDTO);
		}
		
		
		return postDTOList;
	}

	
//	AGGIUNGO UN POST
	@PostMapping(value="/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public PostDTO save(@RequestBody @Valid PostDTO postDTO) throws SavingPostException, UserNotFoundException, StructureNotFoundException {
		
		Post post = modelMapper.map(postDTO, Post.class);
		
		post.setHide(false);
//		post.setText(postDTO.getText());
		post.setStructure(structureService.getById(postDTO.getStructure_id()));
		post.setUser(userService.getById(postDTO.getUser_id()));
		
		Date date = new Date();
		  
		post.setPubblicationDate(date);
		post.setData(postDTO.getDataJSON());

		Post postSaved = postService.save(post);	
		postDTO.setId(postSaved.getId());
		
		
		return postDTO;
		
	}
//	ELIMINO UN POST
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	public  ResponseEntity<Post> delete(@PathVariable int id ) throws PostNotFoundException,IllegalArgumentException{
		
		postService.deleteById(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
//	CERCO TUTTI I POST DI UN UTENTE (dal sui ID)
	@RequestMapping(value="/getByUserId/{id}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PostDTO> getByUserId(@PathVariable("id")int userId) throws UserNotFoundException{
	
	List<Post> postList = postService.findByUserId(userId);
	List<PostDTO> postDTOList = new ArrayList<PostDTO>(); 
	
	for (Post post : postList) {
		
		PostDTO postDTO = modelMapper.map(post, PostDTO.class);
		postDTO.setUser_id(post.getUser().getId());
		
		postDTOList.add(postDTO);
		
	}
		
		return postDTOList;
	}
	
//  CERCO TUTTI I POST DI UN UTENTE DAL SUO NOME
	@RequestMapping(value="/getByUserName/{userName}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PostDTO> getByUserName(@PathVariable("name")String userName) throws UserNotFoundException{
	
	List<Post> postList = postService.findByUserName(userName);
	List<PostDTO> postDTOList = new ArrayList<PostDTO>(); 
	
	for (Post post : postList) {
		
		PostDTO postDTO = modelMapper.map(post, PostDTO.class);
		postDTO.setUser_id(post.getUser().getId());

		postDTOList.add(postDTO);		
	}
	
	return postDTOList;
	}
	
	
	
// CERCO TUTTI I POST DOPO UNA CERTA DATA ****(UTILE NELLA HOME INIZIALE -> VEDI TUTTI I POST DI IERI)
	@RequestMapping(value="/getByDateAfter/{date}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PostDTO> getByDateAfter( @PathVariable("date") @DateTimeFormat(pattern="yyyy-MM-dd")  Date date) throws PostNotFoundException{
	
	List<Post> postList = postService.findByPubblicationDateAfter(date);
	List<PostDTO> postDTOList = new ArrayList<PostDTO>(); 
	
	for (Post post : postList) {
		
		PostDTO postDTO = modelMapper.map(post, PostDTO.class);
		postDTOList.add(postDTO);
		
	}
	
		
		return postDTOList;
	}
	
//	CERCA POST DOPO UNA CERTA DATA E DI UNA CERTA STRUTTURA
	@RequestMapping(value="/getByDateAfterAndStructrureId/{date}/{structureId}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PostDTO> getByDateAfterAndStructrureId( @PathVariable("date") @DateTimeFormat(pattern="yyyy-MM-dd")  Date date, @PathVariable int structureId) throws PostNotFoundException, StructureNotFoundException{
	
	List<Post> postList = postService.findByPubblicationDateAfterAndStructureId(date,structureId);
	List<PostDTO> postDTOList = new ArrayList<PostDTO>(); 
	
	for (Post post : postList) {
		
		PostDTO postDTO = modelMapper.map(post, PostDTO.class);
		postDTOList.add(postDTO);
		
	}
		return postDTOList;
	}
	
	
//	CERCA TUTTI I POST DI UN USER(ID) DOPO UNA CERTA DATA
	@RequestMapping(value="/getByDateAfterAndUserId/{date}/{userId}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PostDTO> getByDateAfterAndUserId( @PathVariable("date") @DateTimeFormat(pattern="yyyy-MM-dd")  Date date, @PathVariable int userId) throws PostNotFoundException, UserNotFoundException{
	
	List<Post> postList = postService.findByPubblicationDateAfterAndUserId(date,userId);
	List<PostDTO> postDTOList = new ArrayList<PostDTO>(); 
	
	for (Post post : postList) {
		
		PostDTO postDTO = modelMapper.map(post, PostDTO.class);
		postDTOList.add(postDTO);
		
	}
		return postDTOList;
	}
	
	
//	CERCA TUTTI I POST DI UNO USER DOPO UNA CERTA DATA E DI UNA CERTA STRUTTURA
	@RequestMapping(value="/getByDateAfterAndStructrureIdAndUserId/{date}/{structureId}/{userId}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PostDTO> getByDateAfterAndStructrureIdAndUserId( @PathVariable("date") @DateTimeFormat(pattern="yyyy-MM-dd")  Date date, @PathVariable int structureId, @PathVariable int userId) throws UserNotFoundException,PostNotFoundException, StructureNotFoundException{
	
	List<Post> postList = postService.findByPubblicationDateAfterAndStructureIdAndUserId(date,structureId,userId);
	List<PostDTO> postDTOList = new ArrayList<PostDTO>(); 
	
	for (Post post : postList) {
		
		PostDTO postDTO = modelMapper.map(post, PostDTO.class);
		postDTOList.add(postDTO);
		
	}
		return postDTOList;
	}

//	CERCO TUTTI I POST DI UNO USER(ID) DI UNA CERTA STRUTTURA(ID)
	@RequestMapping(value="/getByStructureIdAndUserId/{structureId}/{userId}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PostDTO> getByStructureIdAndUserId(@PathVariable int structureId, @PathVariable int userId) throws PostNotFoundException, UserNotFoundException, StructureNotFoundException{
	
	List<Post> postList = postService.findByStructureIdAndUserId(structureId,userId);
	List<PostDTO> postDTOList = new ArrayList<PostDTO>(); 
	
	for (Post post : postList) {
		
		PostDTO postDTO = modelMapper.map(post, PostDTO.class);
		postDTOList.add(postDTO);
		
	}
		return postDTOList;
	}
	
	
	//piu che altro qui richiamo gli attributi di una STRUCTURE (quella a cui appartiene un determinato POST)
	@RequestMapping(value="/getAttributesOfAPost/{postId}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AttributeDTO> getAttributesOfAPost(@PathVariable int postId) throws PostNotFoundException, AttributeNotFoundException {
	
	Post post = postService.getById(postId);
	PostDTO postDTO = modelMapper.map(post, PostDTO.class);
	
	postDTO.setIdsAttribute(new ArrayList<Integer>());
	
	List<AttributeStructure> attributeStructureList =  post.getStructure().getAttributeStructuresList();
	
	List<AttributeDTO> attributeDTOList = new ArrayList<AttributeDTO>();
	
	for (AttributeStructure attributeStructure : attributeStructureList) {
		
		
		Attribute attribute = attributeService.getById(attributeStructure.getAttribute().getId());
		
		AttributeDTO attributeDTO = modelMapper.map(attribute, AttributeDTO.class); 
		attributeDTOList.add(attributeDTO);
	} 
		return attributeDTOList;
	}
	
	//Dato un user, ottengo tutti i post che ha selezionato(partecipa, candidato a lavoro,etc...)
	@RequestMapping(value ="/getPostSelectedByUserId/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PostDTO> getPostSelectedByUser(@PathVariable int userId) throws PostNotFoundException, UserNotFoundException {

		List<UserSelectPost> 	userSelectPostsList = userSelectPostService.findByUserId(userId);
		
		List<PostDTO> postDTOList = new ArrayList<PostDTO>();
		
		
		for (UserSelectPost userSelectPost : userSelectPostsList) {
			
			Post post = userSelectPost.getPost();
			PostDTO postDTO = modelMapper.map(post, PostDTO.class);
			postDTO.setStructure_id(post.getStructure().getId());
			postDTO.setUser_id(post.getUser().getId());
			
			postDTOList.add(postDTO);
		}
		
		return postDTOList;
	}

	//Dato un user, ottengo tutti i post che ha selezionato (partecipa, candidato a lavoro,etc...)
		@RequestMapping(value ="/getPostSelectedByUserUsername/{userUsername}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public List<PostDTO> getPostSelectedByUserUsername(@PathVariable String userUsername) throws PostNotFoundException, UserNotFoundException {

			List<UserSelectPost> 	userSelectPostsList = userSelectPostService.findByUserUsername(userUsername);
			
			List<PostDTO> postDTOList = new ArrayList<PostDTO>();
			
			
			for (UserSelectPost userSelectPost : userSelectPostsList) {
				
				Post post = userSelectPost.getPost();
				PostDTO postDTO = modelMapper.map(post, PostDTO.class);
				postDTO.setStructure_id(post.getStructure().getId());
				postDTO.setUser_id(post.getUser().getId());
				
				postDTOList.add(postDTO);
			}
			
			return postDTOList;
		}
		
		//Dato un user, ottengo tutti i post che ha visualizzato
		@RequestMapping(value ="/getPostViewedByUserId/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public List<PostDTO> getPostViewedByUser(@PathVariable int userId) throws  UserViewPostNotFoundException {

			List<UserViewPost> userViewPostsList = userViewPostService.findByUserId(userId);
			
			List<PostDTO> postDTOList = new ArrayList<PostDTO>();
			
			
			for (UserViewPost userViewPost : userViewPostsList) {
				
				Post post = userViewPost.getPost();
				PostDTO postDTO = modelMapper.map(post, PostDTO.class);
				postDTO.setStructure_id(post.getStructure().getId());
				postDTO.setUser_id(post.getUser().getId());
				
				postDTOList.add(postDTO);
			}
			
			return postDTOList;
		}

		
//		**********     MODIFICARE TESTO DI UN POST 		*********
		
		@PostMapping(value = "/modifyTitle",  consumes = MediaType.APPLICATION_JSON_VALUE)
		public PostDTO modifyTitle(@RequestBody PostDTO postDTO) throws PostNotFoundException {
			
			postService.ModifyPostWithQuery(postDTO.getTitle(), postDTO.getId());
			
			Post post = postService.getById(postDTO.getId());
			postDTO = modelMapper.map(post, PostDTO.class);
					
			return postDTO;
		}
 
} 
