package it.unisalento.sbapp.RestControllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.unisalento.sbapp.DTOClasses.ApplicantDTO;
import it.unisalento.sbapp.DomainClasses.Applicant;
import it.unisalento.sbapp.Exception.SavingUserException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Iservice.IApplicantService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;



@RestController
@RequestMapping("/applicant")
public class ApplicantRestController {
	
	@Autowired
	IApplicantService applicantService;
	
	
	private ModelMapper modelMapper = new ModelMapper();
	
//	CERCO UN APPLICANT attraverso il suo ID
	@RequestMapping(value="/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ApplicantDTO get(@PathVariable int id  ) throws UserNotFoundException{

		Applicant applicant = applicantService.getById(id);
		
		// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO
		
		ApplicantDTO applicantDTO = modelMapper.map(applicant, ApplicantDTO.class);
		applicantDTO.setPassword("*******");

		
		
		return applicantDTO;
	}
	
//  CERCO TUTTI GLI APPLICANT
	@RequestMapping(value="/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ApplicantDTO> getAll( ){
		
		List<Applicant> 	applicantList = applicantService.getAll();
		List<ApplicantDTO> applicantDTOList = new ArrayList<ApplicantDTO>(); 
		
		for (Applicant applicant : applicantList) {
			
			ApplicantDTO applicantDTO = modelMapper.map(applicant, ApplicantDTO.class);

			applicantDTOList.add(applicantDTO);
		}
			
		
		return applicantDTOList;
	}

	
	
	// come passare parametri nel body della request
	// quando possiamo farlo ? solo in 1 caso : quando il method è un POST
	
//	AGGIUNGO UN APPLICANT
	@PostMapping(value="/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ApplicantDTO save(@RequestBody @Valid ApplicantDTO applicantDTO) throws SavingUserException {

		Applicant applicant  = modelMapper.map(applicantDTO, Applicant.class);
		
		Date date = new Date();
		applicant.setRegistrationDate(date);	
		
		Applicant administratorSaved = applicantService.save(applicant);
		
		applicantDTO.setId(administratorSaved.getId());
		applicantDTO.setCompanyId(applicant.getCompany().getId());
	
		return applicantDTO;
		
	}
	
//	RIMUOVO UN APPLICANT
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	public  ResponseEntity<Applicant> delete(@PathVariable int id ) throws UserNotFoundException,IllegalArgumentException{
		
		applicantService.delete(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(value="/login/{username}/{password}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ApplicantDTO login(@PathVariable String username,@PathVariable String password  ) throws UserNotFoundException{
		
		Applicant applicant = applicantService.findByUsernameAndPassword(username,password);
		
		// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO
		
		ApplicantDTO applicantDTO = modelMapper.map(applicant, ApplicantDTO.class);
		applicantDTO.setPassword("*******");
		
		return applicantDTO;
	}
	
// RITORNA TUTTI GLI APPLICANT CHE SONO DISABILITATI{/true} O CHE SONO ABILITATI{/false} 	
	@RequestMapping(value="/getDisabled/{disable}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ApplicantDTO> getDisabled(@PathVariable boolean disable ) throws UserNotFoundException{
		
		List<Applicant> 	applicantList = applicantService.getByDisable(disable);
		List<ApplicantDTO> applicantDTOList = new ArrayList<ApplicantDTO>(); 
		
		for (Applicant applicant : applicantList) {
			
			ApplicantDTO applicantDTO = modelMapper.map(applicant, ApplicantDTO.class);

			applicantDTOList.add(applicantDTO);
		}
			
		
		return applicantDTOList;
	}
	
	
//  CERCO TUTTI GLI APPLICANT DI UNA COMPAGNIA
	@RequestMapping(value="/getByCompanyId/{companyId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ApplicantDTO> getByCompaniId(@PathVariable int companyId ) throws UserNotFoundException{
		
		List<Applicant> 	applicantList = applicantService.findByCompanyId(companyId);
		List<ApplicantDTO> applicantDTOList = new ArrayList<ApplicantDTO>(); 
		
		for (Applicant applicant : applicantList) {
			
			ApplicantDTO applicantDTO = modelMapper.map(applicant, ApplicantDTO.class);
			applicantDTO.setCompanyId(companyId);
			applicantDTO.setId(applicant.getId());
		
			applicantDTOList.add(applicantDTO);
		}
			
		
		return applicantDTOList;
	}
}
