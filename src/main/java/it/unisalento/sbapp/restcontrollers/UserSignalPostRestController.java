package it.unisalento.sbapp.RestControllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.unisalento.sbapp.DTOClasses.PostDTO;
import it.unisalento.sbapp.DTOClasses.UserDTO;
import it.unisalento.sbapp.DTOClasses.UserSignalPostDTO;
import it.unisalento.sbapp.DomainClasses.Post;
import it.unisalento.sbapp.DomainClasses.User;
import it.unisalento.sbapp.DomainClasses.UserSelectPost;
import it.unisalento.sbapp.DomainClasses.UserSignalPost;
import it.unisalento.sbapp.Exception.PostNotFoundException;
import it.unisalento.sbapp.Exception.SavingUserSignalPostException;
import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Exception.UserSignalPostNotFoundException;
import it.unisalento.sbapp.Iservice.IPostService;
import it.unisalento.sbapp.Iservice.IUserService;
import it.unisalento.sbapp.Iservice.IUserSignalPostService;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

@RestController
@RequestMapping("/userSignalPost")
public class UserSignalPostRestController {

	@Autowired
	IUserSignalPostService userSignalPostService;

	@Autowired
	IUserService userService;

	@Autowired
	IPostService postService;

	private ModelMapper modelMapper = new ModelMapper();

	// trova tutti i post segnalati dall'utente { id }

	@GetMapping(value = "/signaledPost/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PostDTO> getSignaledPostByUser(@PathVariable("id") int idUser)
			throws UserSignalPostNotFoundException, PostNotFoundException {

		List<UserSignalPost> userSignalPost = userSignalPostService.findPostSignaledByUser(idUser);
		List<PostDTO> listaPost = new ArrayList<PostDTO>();
		for (UserSignalPost element : userSignalPost) {
			UserSignalPostDTO userSignalPostDTO = modelMapper.map(element, UserSignalPostDTO.class);
			Post post = postService.getById(userSignalPostDTO.getPost_id());
			PostDTO postDTO = modelMapper.map(post, PostDTO.class);
			listaPost.add(postDTO);
		}
		return listaPost;
	}

//trova gli utenti che hanno segnalato quel post { id }
	
	@GetMapping(value = "/reportingUsers/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserDTO> getUserSignaledPost(@PathVariable("id") int idPost)
			throws UserSignalPostNotFoundException, UserNotFoundException {

		List<UserSignalPost> userSignalPost = userSignalPostService.findUserSignaledPostByIdPost(idPost);
		List<UserDTO> listaUser = new ArrayList<UserDTO>();
		for (UserSignalPost element : userSignalPost) {
			UserSignalPostDTO userSignalPostDTO = modelMapper.map(element, UserSignalPostDTO.class);
			User user = userService.getById(userSignalPostDTO.getUser_id());
			UserDTO userDTO = modelMapper.map(user, UserDTO.class);
			listaUser.add(userDTO);
		}
		return listaUser;

	}

	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public UserSignalPostDTO get(@PathVariable int id) throws UserSignalPostNotFoundException {

		UserSignalPost userSignalPost = userSignalPostService.getById(id);

		// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO

		UserSignalPostDTO userSignalPostDTO = modelMapper.map(userSignalPost, UserSignalPostDTO.class);

		return userSignalPostDTO;
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserSignalPostDTO> getAll() {

		List<UserSignalPost> userSignalPostsList = userSignalPostService.getAll();
		List<UserSignalPostDTO> userSignalPostDTOList = new ArrayList<UserSignalPostDTO>();

		for (UserSignalPost userSignalPost : userSignalPostsList) {

			UserSignalPostDTO userSignalPostDTO = modelMapper.map(userSignalPost, UserSignalPostDTO.class);

			userSignalPostDTOList.add(userSignalPostDTO);
		}

		return userSignalPostDTOList;
	}

	@PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public UserSignalPostDTO save(@RequestBody @Valid UserSignalPostDTO userSignalPostDTO)
			throws SavingUserSignalPostException, UserNotFoundException, PostNotFoundException {

		UserSignalPost userSignalPost = modelMapper.map(userSignalPostDTO, UserSignalPost.class);

		userSignalPost.setPost(postService.getById(userSignalPostDTO.getPost_id()));
		userSignalPost.setUser(userService.getById(userSignalPostDTO.getUser_id()));

		UserSignalPost userSignalPostSaved = userSignalPostService.save(userSignalPost);
		userSignalPostDTO.setId(userSignalPostSaved.getId());

		return userSignalPostDTO;

	}

	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<UserSignalPost> delete(@PathVariable int id) throws UserSignalPostNotFoundException,IllegalArgumentException {

		userSignalPostService.delete(id);

		return new ResponseEntity<>(HttpStatus.OK);
	}

}
