
package it.unisalento.sbapp.RestControllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.unisalento.sbapp.DTOClasses.AttributeStructureDTO;
import it.unisalento.sbapp.DomainClasses.AttributeStructure;
import it.unisalento.sbapp.Exception.AttributeNotFoundException;
import it.unisalento.sbapp.Exception.AttributeStructureNotFound;
import it.unisalento.sbapp.Exception.SavingAttributeStructureException;
import it.unisalento.sbapp.Exception.StructureNotFoundException;
import it.unisalento.sbapp.Iservice.*;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;



@RestController
@RequestMapping("/attributeStructure")
public class AttributeStructureRestController {
	
	@Autowired
	IAttributeStructureService attributeStructureService;
	
	@Autowired
	IAttributeService attributeService;
	
	@Autowired
	IStructureService structureService;
	
	
	private ModelMapper modelMapper = new ModelMapper();

//	VIENE RAPPRESENTATA LA RELAZIONE DI APPARTENENZA CHE C'E TRA UNA STRUTTURA FISSA E I SUOI ATTRIBUTI
//	NEL PROGETTO CI SARANNO 3 STRUTTURE E AD OGNUNA SARANNO COLLEGATI CIRCA 5-10 ATTRIBUTI
//	per cui questa TABELLA SARA' NELL'ORDINE DEI 30 INGRESSO
	
//	CERCO UNA RELAZIONE -> ATTRIBUTO  APPARTIENE A UNA STRUTTURA dall'ID
	@RequestMapping(value="/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public AttributeStructureDTO get(@PathVariable int id) throws AttributeStructureNotFound{
		
		AttributeStructure attributeStructure = attributeStructureService.getById(id);
		
		// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO
		
		AttributeStructureDTO attributeStructureDTO = modelMapper.map(attributeStructure, AttributeStructureDTO.class);
		
		
		return attributeStructureDTO;
	}
	
//	CERCO TUTTE LE RELAZIONI DI APPARTENENZA
	@RequestMapping(value="/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AttributeStructureDTO> getAll(){
		
		List<AttributeStructure> 	attributeStructureList = attributeStructureService.getAll();
		List<AttributeStructureDTO> attributeStructureDTOList = new ArrayList<AttributeStructureDTO>(); 
		
		for (AttributeStructure attributeStructure : attributeStructureList) {
			
			AttributeStructureDTO attributeStructureDTO = modelMapper.map(attributeStructure, AttributeStructureDTO.class);
			
			
			attributeStructureDTOList.add(attributeStructureDTO);
		}
		
		
		return attributeStructureDTOList;
	}

	
// AGGIUNGO UNA RELAZIONE -> ATTRIBUTE APPARTIENE AD UNA STRUTTURA	
	@PostMapping(value="/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public AttributeStructureDTO save(@RequestBody @Valid AttributeStructureDTO attributeStructureDTO) throws SavingAttributeStructureException, AttributeNotFoundException, StructureNotFoundException {
		
		AttributeStructure attributeStructure = modelMapper.map(attributeStructureDTO, AttributeStructure.class);
		
		attributeStructure.setAttribute(attributeService.getById(attributeStructureDTO.getStructure_id()));
		attributeStructure.setStructure(structureService.getById(attributeStructureDTO.getStructure_id()));

		AttributeStructure attributeStructureSaved = attributeStructureService.save(attributeStructure);	
		attributeStructureDTO.setId(attributeStructureSaved.getId());
		
		return attributeStructureDTO;
		
	}

//	RIMUOVO UNA RELAZIONE -> ATTRIBUTO NON APPARTIENE PIU' AD UNA STRUTTURA
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	public  ResponseEntity<AttributeStructure> delete(@PathVariable int id ) throws AttributeStructureNotFound,IllegalArgumentException{
		
		attributeStructureService.delete(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
//	CERCO TUTTE LE RELAZIONI ATTRIBUTO-STRUTTURA DALL'id DELLA STRUTTURA
//  Esempio: Voglio sapere tutti gli attributi possibili della struttura con id : 1
	@RequestMapping(value="/getByStructureId/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AttributeStructureDTO> getByStructureId(@PathVariable int id) throws AttributeStructureNotFound{
		
		List<AttributeStructure> attributeStructureList = attributeStructureService.findByStructureId(id);
		List<AttributeStructureDTO> attributeStructureDTOList = new ArrayList<AttributeStructureDTO>(); 
		
		for (AttributeStructure attributeStructure : attributeStructureList) {
			AttributeStructureDTO attributeStructureDTO = modelMapper.map(attributeStructure, AttributeStructureDTO.class);
			
			attributeStructureDTO.setAttribute_id(attributeStructure.getAttribute().getId());
			attributeStructureDTO.setStructure_id(attributeStructure.getStructure().getId());
			
			attributeStructureDTOList.add(attributeStructureDTO);
		}
		
		
		return attributeStructureDTOList;
	}

//	CERCO TUTTE LE RELAZIONI ATTRIBUTO-STRUTTURA DALL'id DELLA STRUTTURA
//  Esempio: Voglio sapere tutti gli attributi possibili della struttura di nome : Job Offer
	@RequestMapping(value="/getByStructureTitle/{title}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AttributeStructureDTO> getByStructureTitle(@PathVariable String title) throws AttributeStructureNotFound{
		
		List<AttributeStructure> attributeStructureList = attributeStructureService.findByStructureTitle(title);
		List<AttributeStructureDTO> attributeStructureDTOList = new ArrayList<AttributeStructureDTO>(); 
		
		for (AttributeStructure attributeStructure : attributeStructureList) {
			
			AttributeStructureDTO attributeStructureDTO = modelMapper.map(attributeStructure, AttributeStructureDTO.class);
			attributeStructureDTO.setAttribute_id(attributeStructure.getAttribute().getId());
			attributeStructureDTO.setStructure_id(attributeStructure.getStructure().getId());
			
			attributeStructureDTOList.add(attributeStructureDTO);
		}
		
		
		return attributeStructureDTOList;
	}
	
}
