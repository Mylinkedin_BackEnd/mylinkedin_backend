package it.unisalento.sbapp.RestControllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import it.unisalento.sbapp.DTOClasses.MessageDTO;
import it.unisalento.sbapp.DomainClasses.Message;
import it.unisalento.sbapp.Exception.MessageNotFoundException;
import it.unisalento.sbapp.Exception.SavingMessageException;

import it.unisalento.sbapp.Exception.UserNotFoundException;
import it.unisalento.sbapp.Iservice.*;

@RestController
@RequestMapping("/message")
public class MessageRestController {
	
	@Autowired
	IMessageService messageService;

	@Autowired
	IUserService userService;
	
	private ModelMapper modelMapper = new ModelMapper();
	
	@RequestMapping(value="/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public MessageDTO get(@PathVariable int id) throws MessageNotFoundException{
		
		Message message = messageService.getById(id);
		
		// Abbiamo un oggetto IUserDTO da mappare in un oggetto userDTO
		
		MessageDTO messageDTO = modelMapper.map(message, MessageDTO.class);
		
//		messageDTO.setText(message.getText());
//		messageDTO.setDate(message.getDate());
//		messageDTO.setId(message.getId());
//		messageDTO.setUser_receiver_id(message.getUserReceiver().getId());
//		messageDTO.setUser_sender_id(message.getUserSender().getId());
		
		return messageDTO;
	}
	
//  CERCO TUTTI I MESSAGGI
	@RequestMapping(value="/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<MessageDTO> getAll(){
		
		List<Message> 	messageList = messageService.getAll();
		List<MessageDTO> messageDTOList = new ArrayList<MessageDTO>(); 
		
		for (Message message : messageList) {
			
			MessageDTO messageDTO = modelMapper.map(message, MessageDTO.class);
			
//			messageDTO.setText(message.getText());
//			messageDTO.setDate(message.getDate());
//			messageDTO.setId(message.getId());
//			messageDTO.setUser_receiver_id(message.getUserReceiver().getId());
//			messageDTO.setUser_sender_id(message.getUserSender().getId());
			
			messageDTOList.add(messageDTO);
		}
		
		
		return messageDTOList;
	}

	
//	AGGIUNGO UN MESSAGGIO
	@PostMapping(value="/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public MessageDTO save(@RequestBody @Valid MessageDTO messageDTO) throws SavingMessageException, UserNotFoundException {
		
		Message message = modelMapper.map(messageDTO, Message.class);

		
		
//		message.setText(messageDTO.getText());
		message.setUserReceiver(userService.getById(messageDTO.getUser_receiver_id()));
		message.setUserSender(userService.getById(messageDTO.getUser_sender_id()));
		message.setMessage_Read(false);
		
		Date date = new Date();
		  
		 message.setSentDate(date);
		 
		
		Message messageSaved = messageService.save(message);	
		messageDTO.setId(messageSaved.getId());
		
		return messageDTO;
		
	}

//	RIMUOVO UN MESSAGGIO DAL SUO ID
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	public  ResponseEntity<Message> delete(@PathVariable int id ) throws MessageNotFoundException, IllegalArgumentException{
		
		messageService.delete(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
//  CERCO TUTTI I MESSAGGI RICEVUTI DA UN UTENTE (tramite l'ID dell'utente)	
	@RequestMapping(value="/getByReceiver/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<MessageDTO> getByReceiverId(@PathVariable int id) throws UserNotFoundException{
		
		List<Message> 	messageList = messageService.findByUserReceiverId(id);
		List<MessageDTO> messageDTOList = new ArrayList<MessageDTO>(); 
		
		for (Message message : messageList) {
			
			MessageDTO messageDTO = modelMapper.map(message, MessageDTO.class);

			messageDTOList.add(messageDTO);
		}
		
		return messageDTOList;
	}
	
//  CERCO TUTTI I MESSAGGI INVIATI DA UN UTENTE (tramite l'ID dell'utente)		
	@RequestMapping(value="/getBySender/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<MessageDTO> getBySenderId(@PathVariable int id) throws UserNotFoundException{
		
		List<Message> 	messageList = messageService.findByUserSenderId(id);
		List<MessageDTO> messageDTOList = new ArrayList<MessageDTO>(); 
		
		for (Message message : messageList) {
			
			MessageDTO messageDTO = modelMapper.map(message, MessageDTO.class);

			messageDTOList.add(messageDTO);
		}
		
		return messageDTOList;
	}

//	CERCO TUTTI I MESSAGGI RICEVUTI DA UN UTENTE (dal suo ID)E NON ANCORA VISTI
//	UTILE NEL MOSTRARE IN SOVRIMPRESSIONE O DIVERSAMENTE I MESSAGGI NON ANCORA LETTI
	@RequestMapping(value="/getByReceiverAndMessageRead/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<MessageDTO> getByReceiverAndMessageRead(@PathVariable int id) throws UserNotFoundException{
		
		List<Message> 	messageList = messageService.findByUserReceiverIdAndMessageRead(id,false);
		List<MessageDTO> messageDTOList = new ArrayList<MessageDTO>(); 
		
		for (Message message : messageList) {
			
			MessageDTO messageDTO = modelMapper.map(message, MessageDTO.class);

			messageDTOList.add(messageDTO);
		}
		
		return messageDTOList;
	}
	
//	CERCO I MESSAGGI SCAMBIATI TRA DUE PRECISI UTENTI (Attento!!! è solo in un verso)
	@RequestMapping(value="/getBySenderIdAndReceiverId/{senderId}/{receiverId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<MessageDTO> getBySenderIdAndReceiverId(@PathVariable int senderId, @PathVariable int receiverId) throws UserNotFoundException{
		
		List<Message> 	messageList = messageService.findByUserReceiverIdAndUserSenderId(receiverId,senderId);
		List<MessageDTO> messageDTOList = new ArrayList<MessageDTO>(); 
		
		for (Message message : messageList) {
			
			MessageDTO messageDTO = modelMapper.map(message, MessageDTO.class);

			messageDTOList.add(messageDTO);
		}
		
		return messageDTOList;
	}
	
	
//	CERCO I MESSAGGI SCAMBIATI TRA DUE PRECISI UTENTI (Attento!!! è solo in un verso)
	@RequestMapping(value="/getBySenderUsernameAdReceiverUsername/{senderUsername}/{receiverUsername}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<MessageDTO> getBySenderUsernameAndReceiverUsername(@PathVariable String senderUsername, @PathVariable String receiverUsername) throws UserNotFoundException{
		
		List<Message> 	messageList = messageService.findByUserReceiverUsernameLikeAndUserSenderUsernameLike(receiverUsername,senderUsername);
		List<MessageDTO> messageDTOList = new ArrayList<MessageDTO>(); 
		
		for (Message message : messageList) {
			
			MessageDTO messageDTO = modelMapper.map(message, MessageDTO.class);

			messageDTOList.add(messageDTO);
		}
		
		return messageDTOList;
	}
	
//  CERCO TUTTI I MESSAGGI INVIATI DA ME AD UN UTENTE (tramite l'USERNAME dell'utente e il mio ID)	
	@RequestMapping(value="/getByReceiverUsernameAndSenderId/{username}/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<MessageDTO> getByReceiverUsernameAndSenderId(@PathVariable String username, @PathVariable int id) throws UserNotFoundException{
		
		List<Message> 	 messageList = messageService.findByUserReceiverUsernameAndUserSenderId(username,id);
		List<MessageDTO> messageDTOList = new ArrayList<MessageDTO>(); 
		
		for (Message message : messageList) {
			
			MessageDTO messageDTO = modelMapper.map(message, MessageDTO.class);

			messageDTOList.add(messageDTO);
		}
		
		return messageDTOList;
	}
}
